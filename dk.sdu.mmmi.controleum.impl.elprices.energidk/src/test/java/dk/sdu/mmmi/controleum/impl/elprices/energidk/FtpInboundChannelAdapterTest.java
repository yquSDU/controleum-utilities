/*
 * Copyright 2002-2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import org.apache.log4j.Logger;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;

/**
 * This test will use the 2 files previously uploaded. Using an Inbound Channel
 * Adapter, the test will poll the remote (Root) directory that will contain 2
 * files. The adapter will attempt to transfer them to a local directory, which
 * will be generated. Once copied, the files will be sent as a payload of the
 * message to a channel. We are using a file filter, transferring only files
 * that end with 'txt'. The remote files are not deleted.
 *
 * @author Oleg Zhurakousky
 * @author Gunnar Hillert
 *
 */
public class FtpInboundChannelAdapterTest {

    private static final Logger LOGGER = Logger.getLogger(FtpInboundChannelAdapterTest.class);

    @Test
    public void runDemo() throws Exception {
        ConfigurableApplicationContext ctx
                = new ClassPathXmlApplicationContext("META-INF/spring/FtpInboundChannelAdapter-context.xml");

        PollableChannel ftpChannel = ctx.getBean("ftpChannel", PollableChannel.class);

        Message<?> message1 = ftpChannel.receive(2000);

        LOGGER.info(String.format("Received first file message: %s.", message1));

        assertNotNull(message1);

        ctx.close();
    }
}
