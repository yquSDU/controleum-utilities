package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;

public class FileInboundTransformerTest {

    private static final Logger LOGGER = Logger
            .getLogger(FileInboundTransformerTest.class);

    @Test
    public void testTransform() throws IOException, InterruptedException {

        // Setup:
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext(
                "META-INF/spring/FileInboundChannelAdapter-context.xml");

        // Test:
        PollableChannel fileChannel = ctx.getBean(
                "createEnergiDkDataRequestChannel", PollableChannel.class);

        Message<?> forecastMsg1 = fileChannel.receive();

        LOGGER.info(String.format(
                "Received first file transformer message: %s.",
                forecastMsg1.getHeaders()));

        EnergiDkForecastData forecast = (EnergiDkForecastData) forecastMsg1.getPayload();

        System.out.println(forecast);

        // Asserts:
        ctx.close();
    }
}
