/*
 * Copyright 2002-2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.io.File;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

/**
 * This sample will take 2 local files
 *
 * 1. a.txt 2. b.txt
 *
 * and transfer them to a remote directory '/'.
 *
 * @author Oleg Zhurakousky
 * @author Gunnar Hillert
 *
 */
public class FtpOutboundChannelAdapterTest {

    private static final Logger LOGGER = Logger.getLogger(FtpOutboundChannelAdapterTest.class);

    private final File baseFolder = new File("target" + File.separator + "toSend");

    @Test
    public void runDemo() throws Exception {

        ConfigurableApplicationContext ctx
                = new ClassPathXmlApplicationContext("META-INF/spring/FtpOutboundChannelAdapter-context.xml");

        MessageChannel ftpChannel = ctx.getBean("ftpChannel", MessageChannel.class);

        baseFolder.mkdirs();

        final File fileToSendA = new File(baseFolder, "spotprisprognose.csv");

        final InputStream inputStreamA = FtpOutboundChannelAdapterTest.class.getResourceAsStream("/test-files/spotprisprognose.csv");

        FileUtils.copyInputStreamToFile(inputStreamA, fileToSendA);

        assertTrue(fileToSendA.exists());

        final Message<File> messageA = MessageBuilder.withPayload(fileToSendA).build();

        ftpChannel.send(messageA);

        Thread.sleep(2000);

        assertTrue(new File(TestSuite.FTP_ROOT_DIR + File.separator + "spotprisprognose.csv").exists());

        LOGGER.info("Successfully transfered file 'spotprisprognose.csv' to a remote FTP location.");
        ctx.close();
    }

    @After
    public void cleanup() {
        FileUtils.deleteQuietly(baseFolder);
    }

}
