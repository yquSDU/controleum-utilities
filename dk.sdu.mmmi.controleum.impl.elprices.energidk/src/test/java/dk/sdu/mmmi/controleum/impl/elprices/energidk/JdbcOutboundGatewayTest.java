package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.util.Date;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The test class for jdbc outbound gateway
 *
 * @author JCS
 *
 */
public class JdbcOutboundGatewayTest {

    private Logger logger = Logger.getLogger(JdbcOutboundGatewayTest.class);

    @Test
    public void insertEnergiDkDataRecord() {

        // Setup:
        ApplicationContext context
                = new ClassPathXmlApplicationContext("META-INF/spring/JdbcOutboundGateway-context.xml");

        // Test:
        EnergiDkForecastService service = context.getBean(EnergiDkForecastService.class);
        logger.info("Creating energiDk data Instance");

        Date timest = new Date();
        EnergiDkForecastRecord r = new EnergiDkForecastRecord();
        r.setForecastTime(timest);
        r.setLocalTime(timest);
        r.setCurrency("DK");
        r.setPriceArea("DK1");
        r.setLocalPrice(200.00);

        r = service.createEnergiDkData(r);

        Assert.assertNotNull("Expected a non null instance of EnergiDkData, got null", r);
        logger.info("\n\tGenerated person with time: " + r.getLocalTime() + ", with price : " + r.getLocalPrice());
    }
}
