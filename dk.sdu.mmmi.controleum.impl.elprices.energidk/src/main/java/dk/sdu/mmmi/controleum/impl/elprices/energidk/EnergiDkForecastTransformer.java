package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import com.csvreader.CsvReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author jcs
 */
public class EnergiDkForecastTransformer {

    private final static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    private final static DecimalFormat df = new DecimalFormat("###,##");

    public EnergiDkForecastData transform(File input) throws IOException, ParseException {

        CsvReader csvReader = new CsvReader(new FileReader(input), ';');
        csvReader.readHeaders();

        long forecastTime = input.lastModified();
        EnergiDkForecastData forecast = new EnergiDkForecastData(input.lastModified());

        while (csvReader.readRecord()) {

            Date localTime = sdf.parse(csvReader.get("lokaldatotid"));
            String priceArea = csvReader.get("prisomraade");
            Number localPrice = df.parse(csvReader.get("lokalpris"));

            EnergiDkForecastRecord dr = new EnergiDkForecastRecord();
            dr.setForecastTime(new Date(forecastTime));
            dr.setLocalTime(localTime);
            dr.setPriceArea(priceArea);
            dr.setLocalPrice(localPrice.doubleValue());

            forecast.addDataRecord(dr);
        }

        return forecast;
    }
}
