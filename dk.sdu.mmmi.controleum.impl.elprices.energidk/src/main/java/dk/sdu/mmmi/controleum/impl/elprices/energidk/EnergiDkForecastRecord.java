package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.util.Date;

/**
 *
 * @author jcs
 */
public class EnergiDkForecastRecord {

    private Date forecastTime;
    private Date localTime;
    private String priceArea = "DK1";
    private Double localPrice = 0.00;
    private String currency = "DDK";

    public void setLocalPrice(Double localPrice) {
        this.localPrice = localPrice;
    }

    public Double getLocalPrice() {
        return localPrice;
    }

    public void setLocalTime(Date localTime) {
        this.localTime = localTime;
    }

    public Date getLocalTime() {
        return this.localTime;
    }

    public void setPriceArea(String priceArea) {
        this.priceArea = priceArea;
    }

    public String getPriceArea() {
        return this.priceArea;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return this.currency;
    }

    public Date getForecastTime() {
        return forecastTime;
    }

    void setForecastTime(Date forecastTime) {
        this.forecastTime = forecastTime;
    }
}
