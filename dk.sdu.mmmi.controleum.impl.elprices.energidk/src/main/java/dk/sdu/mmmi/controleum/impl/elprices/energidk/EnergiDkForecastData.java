package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public class EnergiDkForecastData {

    private final Date timeStamp;
    private final List<EnergiDkForecastRecord> dataRecords = new ArrayList<>();

    public EnergiDkForecastData(Date fileLastModified) {
        this.timeStamp = fileLastModified;
    }

    EnergiDkForecastData(long fileLastModified) {
        this.timeStamp = new Date(fileLastModified);
    }

    public void addDataRecord(EnergiDkForecastRecord dataRecord) {
        this.dataRecords.add(dataRecord);
    }

}
