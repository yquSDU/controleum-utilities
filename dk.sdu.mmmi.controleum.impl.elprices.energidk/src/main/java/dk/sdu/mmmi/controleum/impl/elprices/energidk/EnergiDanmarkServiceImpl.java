package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import java.util.Date;
import java.util.Map;
import org.openide.util.lookup.ServiceProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = EnergyPriceService.class)
public class EnergiDanmarkServiceImpl implements EnergyPriceService {

    private final ApplicationContext context
            = new ClassPathXmlApplicationContext(
                    "META-INF/spring/JdbcOutboundGateway-context.xml",
                    "META-INF/spring/FtpOutboundChannelAdapter-context.xml",
                    "META-INF/spring/FtpInboundChannelAdapter-context.xml",
                    "META-INF/spring/FileInboundChannelAdapter-context.xml");

    public EnergiDanmarkServiceImpl() {

    }

    @Override
    public Map<Date, Double> getPrices(EnergyPriceDatabaseArea area,
            Date startDateTime, Date endDateTime) throws EnergyPriceDatabaseException {

        throw new UnsupportedOperationException("Not supported yet.");
    }

}
