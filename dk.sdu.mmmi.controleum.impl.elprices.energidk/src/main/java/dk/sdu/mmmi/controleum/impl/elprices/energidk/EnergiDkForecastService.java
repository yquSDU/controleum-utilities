package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public interface EnergiDkForecastService {

    EnergiDkForecastRecord createEnergiDkData(EnergiDkForecastRecord data);

    List<EnergiDkForecastRecord> findEnergiDkForecastRecordByPeriod(String location, Date from, Date to);
}
