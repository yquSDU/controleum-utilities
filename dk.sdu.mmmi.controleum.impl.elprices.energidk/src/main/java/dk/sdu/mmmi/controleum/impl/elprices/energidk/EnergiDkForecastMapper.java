package dk.sdu.mmmi.controleum.impl.elprices.energidk;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author jcs
 */
public class EnergiDkForecastMapper implements RowMapper<EnergiDkForecastRecord> {

    @Override
    public EnergiDkForecastRecord mapRow(ResultSet rs, int rowNum) throws SQLException {

        EnergiDkForecastRecord dr = new EnergiDkForecastRecord();
        dr.setPriceArea(rs.getString("location"));
        dr.setLocalPrice(rs.getDouble("price"));
        dr.setLocalTime(rs.getDate("timest"));
        dr.setForecastTime(rs.getDate("forecast_timest"));
        dr.setCurrency(rs.getString("currency"));
        return dr;
    }

}
