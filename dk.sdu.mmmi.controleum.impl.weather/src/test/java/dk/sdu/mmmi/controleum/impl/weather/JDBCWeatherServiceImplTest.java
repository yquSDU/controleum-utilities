/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.weather;

import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.weather.JDBCWeatherServiceImpl;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jcs
 */
public class JDBCWeatherServiceImplTest {

    private static JDBCWeatherServiceImpl service;

    public JDBCWeatherServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        service = new JDBCWeatherServiceImpl();
    }

    @AfterClass
    public static void tearDownClass() {
        service.dispose();
    }

    /**
     * Test of getOutdoorLightForecast method, of class JDBCWeatherServiceImpl.
     */
    @Test
    public void testGetOutdoorLightForecast() throws Exception {
        System.out.println("getOutdoorLightForecast");
        // SETUP
        LightForecastConfig cfg = new LightForecastConfig();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm");
        Date from = df.parse("05-09-2013 00:00");
        Date to = df.parse("05-09-2013 23:00");

        // TEST
        Sample expResult = new Sample(from, new OutdoorLightForecast());

        Sample<OutdoorLightForecast> result = service.getOutdoorLightForecast(cfg, from, to);

        // ASSERTS
        Assert.assertEquals(expResult.getTimestamp(), result.getTimestamp());
        Assert.assertEquals(24, result.getSample().get().size());
    }
}
