package dk.sdu.mmmi.controleum.impl.weather;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.api.weather.IWeatherServiceDriver;
import dk.sdu.mmmi.controleum.api.weather.Log;
import dk.sdu.mmmi.controleum.api.weather.WeatherCommunicationException;
import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.TemperatureForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorTemperatureForecast;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTimeZone;
import org.openide.util.lookup.ServiceProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = IWeatherServiceDriver.class)
public class JDBCWeatherServiceImpl implements IWeatherServiceDriver, Disposable {

    private final AbstractApplicationContext context;
    private static final int UNIFIED_CODE_LIGHT = 56;
    private static final int UNIFIED_CODE_TEMPERATURE = 55;
    private static final int OFFSET = 0;

    public JDBCWeatherServiceImpl() {
        this.context = new AnnotationConfigApplicationContext(JDBCWeatherServiceConfig.class);
    }

    @Override
    public Sample<OutdoorLightForecast> getOutdoorLightForecast(LightForecastConfig cfg, Date from, Date to)
            throws WeatherCommunicationException {

        final DateTimeZone localTimeZone = DateTimeZone.getDefault();

        long utcFrom = localTimeZone.convertLocalToUTC(from.getTime(), true);
        long utcTo = localTimeZone.convertLocalToUTC(to.getTime(), true);

        String table = String.format("%s_uc%s_d%s", cfg.getLocation(), UNIFIED_CODE_LIGHT, OFFSET);
        List<Log> samples = context.getBean(JdbcTemplate.class).
                query("SELECT * FROM " + table
                        + " WHERE timest >= ? AND timest <= ? ORDER BY timest",
                        rowMapper, utcFrom, utcTo);

        // Map data
        List<Sample<WattSqrMeter>> convertedForecast = new ArrayList();
        for (Log log : samples) {

            Date date = new Date(localTimeZone.convertUTCToLocal(log.getTime()));
            WattSqrMeter wattSqrMeter = new WattSqrMeter(log.getValue());
            // Convert forecast from utc time
            convertedForecast.add(new Sample(
                    new Date(localTimeZone.convertUTCToLocal(log.getTime())),
                    new WattSqrMeter(log.getValue())));
        }

        return new Sample(from, new OutdoorLightForecast(convertedForecast));
    }
    
    
    @Override
    public Sample<OutdoorTemperatureForecast> getOutdoorTemperatureForecast(TemperatureForecastConfig cfg, Date from, Date to) {

        final DateTimeZone localTimeZone = DateTimeZone.getDefault();

        long utcFrom = localTimeZone.convertLocalToUTC(from.getTime(), true);
        long utcTo = localTimeZone.convertLocalToUTC(to.getTime(), true);

        String table = String.format("%s_uc%s_d%s", cfg.getLocation(), UNIFIED_CODE_TEMPERATURE, OFFSET);
        List<Log> samples = context.getBean(JdbcTemplate.class).
                query("SELECT * FROM " + table
                        + " WHERE timest >= ? AND timest <= ? ORDER BY timest",
                        rowMapper, utcFrom, utcTo);

        // Map data
        List<Sample<Celcius>> convertedForecast = new ArrayList();
        for (Log log : samples) {

            // Convert forecast from utc time
            convertedForecast.add(new Sample(
                    new Date(localTimeZone.convertUTCToLocal(log.getTime())),
                    new Celcius(log.getValue())));
        }

        return new Sample(from, new OutdoorTemperatureForecast(convertedForecast));
    }
    
    
    /**
     *
     */
    private final RowMapper<Log> rowMapper = new RowMapper<Log>() {
        @Override
        public Log mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Log(rs.getLong("timest"), rs.getDouble("value"));
        }
    };

    public ApplicationContext getContext() {
        return context;
    }

    @Override
    public void dispose() {
        context.close();
    }
}
