/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr.api;

import java.util.GregorianCalendar;

/**
 *
 * @author ancla
 */
public class OpenADRAction<T> {
    private final String name;
    private final Float signal;
    private final GregorianCalendar dtStart;
    private final long length;
    
    public OpenADRAction(String name, Float signal, GregorianCalendar dtStart, long length)
    {
        this.name = name;
        this.signal = signal;
        this.dtStart = dtStart;
        this.length = length;
    }
    
    public Float getSignal()
    {
        return this.signal;
    }
    
    public String getName()
    {
        return this.name;
    }

    /**
     * @return the dtStart
     */
    public GregorianCalendar getStartCalendar() {
        return dtStart;
    }

    /**
     * @return the length
     */
    public long getLength() {
        return length;
    }
}
