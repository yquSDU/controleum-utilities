/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr;

import com.decouplink.Disposable;
import static com.decouplink.Utilities.context;
import com.enernoc.open.oadr2.model.v20b.OadrDistributeEvent.OadrEvent;
import com.enernoc.open.oadr2.model.v20b.OadrSignedObject;
import com.enernoc.open.oadr2.model.v20b.ei.EiEventSignal;
import com.enernoc.open.oadr2.model.v20b.ei.Interval;
import com.enernoc.open.oadr2.model.v20b.ei.PayloadFloatType;
import com.enernoc.open.oadr2.model.v20b.ei.SignalPayload;
import com.enernoc.open.oadr2.xmpp.OADR2PacketExtension;
import com.enernoc.open.oadr2.xmpp.v20b.OADR2PacketFilter;
import com.enernoc.open.oadr2.xmpp.v20b.XMLNS;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.impl.openadr.api.OpenADRAction;
import dk.sdu.mmmi.controleum.impl.openadr.api.OpenADRConnector;
import dk.sdu.mmmi.controleum.impl.openadr.api.OpenADRSubscriber;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Packet;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.openide.util.Exceptions;

/**
 *
 * @author ancla
 */
public class OpenADRConnectorImpl implements PacketListener, Disposable, OpenADRConnector {

    private final ControleumContext cd;
    static String username = "sdu_openadr";
    static String passwd = "sdu_openadr";
    static String hostname = "localhost";
    static int port = 5222;

    ConnectionConfiguration connConfig = new ConnectionConfiguration(hostname, port);
    XMPPConnection venConnection;

    //DR Message queue
    BlockingQueue<Packet> packets = new ArrayBlockingQueue<>(10);

    public OpenADRConnectorImpl(ControleumContext cd) {

        this.cd = cd;
    }

    public void setConfig(OpenADRXMPPConfig cfg) {
        this.username = cfg.getUsername();
        this.hostname = cfg.getHostname();
        this.passwd = cfg.getPasswd();
        this.port = cfg.getPort();
        this.connConfig = new ConnectionConfiguration(hostname, port);
        if (venConnection.isConnected()) {
            this.venConnection.disconnect();
            try {
                connect();
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    public void connect() throws Exception {
        this.venConnection = connect("ven");
        venConnection.addPacketListener(this, new OADR2PacketFilter());
    }

    private XMPPConnection connect(String resource) throws XMPPException {
        XMPPConnection c = new XMPPConnection(connConfig);

        c.connect();
        c.login(username, passwd, resource);
//        Presence presence = new Presence(Presence.Type.available);
//        c.sendPacket(presence);
        return c;
    }

    @Override
    public void processPacket(Packet packet) {
        int errorCode = validatePackage(packet);
        if (errorCode == 1) {
            OADR2PacketExtension extension = (OADR2PacketExtension) packet.getExtension(XMLNS.OADR2.getNamespaceURI());
            OadrSignedObject payload = (OadrSignedObject) extension.getPayload();
            Collection<? extends OpenADRSubscriber> subscribers = context(cd).all(OpenADRSubscriber.class);
            //For each event in this openADR package
            for (OadrEvent e : payload.getOadrDistributeEvent().getOadrEvents()) {
                GregorianCalendar dtStart = e.getEiEvent().getEiActivePeriod().getProperties().getDtstart().getDateTime().getValue().toGregorianCalendar();
                for (EiEventSignal es : e.getEiEvent().getEiEventSignals().getEiEventSignals()) {
                    List<Float> values = new ArrayList<>();

                    for (Interval v : es.getIntervals().getIntervals()) {
                        SignalPayload p = (SignalPayload) v.getStreamPayloadBases().get(0).getValue();
                        PayloadFloatType pl = (PayloadFloatType) p.getPayloadBase().getValue();
                        Duration d = Duration.parse(v.getDuration().getDuration().getValue());
                        long period = d.getSeconds() * 1000 + d.getNano() / 1000000;
                        OpenADRAction a = new OpenADRAction(es.getSignalName(), pl.getValue(), dtStart, period);
                        for (OpenADRSubscriber s : subscribers) {
                            if (s.getFilter().accept(es)) {
                                s.notify(a);
                            }
                        }
                        dtStart.setTimeInMillis(dtStart.getTimeInMillis() + period);
                    }
                }
            }

        }
    }

    @Override
    public void dispose() {
        if (this.venConnection != null && venConnection.isConnected()) {
            venConnection.disconnect();
        }
    }

    private int validatePackage(Packet packet) {
        //TODO Validate package according to OpenADR 2.0b specification
        return 1;
    }

    public void sendMessage(Packet packet) {
        venConnection.sendPacket(packet);
    }

    @Override
    public boolean isConnected() {
        return venConnection == null ? false : venConnection.isConnected();
    }

}
