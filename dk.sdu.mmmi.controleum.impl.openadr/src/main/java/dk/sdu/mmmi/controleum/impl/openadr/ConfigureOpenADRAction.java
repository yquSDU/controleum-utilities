/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.impl.openadr.api.OpenADRConnector;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

/**
 *
 * @author ancla
 */
public class ConfigureOpenADRAction  extends AbstractAction implements HelpCtx.Provider, Presenter.Popup {

    private final ControleumContext g;

    public ConfigureOpenADRAction(ControleumContext g) {
        super("Configure OpenADR XMPP");
        this.g = g;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public boolean isEnabled() {
        return g != null
                && context(g).one(OpenADRConnector.class) != null
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling()
                && !getControlManager().isAutoRunning();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // Panel.
        SLConfigPanel pnl = new SLConfigPanel();

        // Dialog.
        DialogDescriptor dcs = new DialogDescriptor(
                pnl,
                "Configure OpenADR XMPP");
        //TODO Persistance
        OpenADRXMPPConfig cfg = new OpenADRXMPPConfig();
        pnl.set(cfg);

        Object result = DialogDisplayer.getDefault().notify(dcs);
        
        //TODO Persistance
//        if (result == DialogDescriptor.OK_OPTION) {
//            db.setSuperLinkConfiguration(pnl.get());
//        }
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenuItem mi = new JMenuItem(this);
        mi.setIcon(ImageUtilities.loadImageIcon("dk/sdu/mmmi/gc/silk/wrench.png", false));
        return mi;
    }

    private ControlManager getControlManager() {
        return context(g).one(ControlManager.class);
    }

    /**
     * Configuration panel.
     */
    private static class SLConfigPanel extends JPanel {

        private final JTextField hostname = new JTextField(),
                port = new JTextField(),
                username = new JTextField(),
                password = new JTextField();

        public SLConfigPanel() {
            setLayout(new GridLayout(6, 2, 2, 2));
            add(new JLabel("Hostname:"));
            add(hostname);
            add(new JLabel("Port:"));
            add(port);
            add(new JLabel("Username:"));
            add(username);
            add(new JLabel("Password:"));
            add(password);

        }

        public void set(OpenADRXMPPConfig cfg) {
            hostname.setText(cfg.getHostname());
            port.setText("" + cfg.getPort());
            username.setText(cfg.getUsername());
            password.setText(cfg.getPasswd());
        }

        public OpenADRXMPPConfig get() {
            OpenADRXMPPConfig r = new OpenADRXMPPConfig();
            r.setHostname(hostname.getText());
            r.setPort(Integer.parseInt(port.getText()));
            r.setUsername(username.getText());
            r.setPasswd(password.getText());
            return r;
        }
    }
}
