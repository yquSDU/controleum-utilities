/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr.api.filter;

import com.enernoc.open.oadr2.model.v20b.ei.EiEventSignal;

/**
 *
 * @author ancla
 */
public interface OpenADREventFilter {
    public boolean accept(EiEventSignal p);
}
