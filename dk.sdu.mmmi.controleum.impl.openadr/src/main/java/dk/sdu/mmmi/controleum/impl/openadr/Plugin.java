/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr;
import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
/**
 *
 * @author ancla
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControleumContext cd) {
        DisposableList d = new DisposableList();
        //TODO Fetch DB config
        OpenADRConnectorImpl s = new OpenADRConnectorImpl(cd);
        try {
            d.add(context(cd).add(OpenADRConnectorImpl.class, s));
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return d;
    }
}