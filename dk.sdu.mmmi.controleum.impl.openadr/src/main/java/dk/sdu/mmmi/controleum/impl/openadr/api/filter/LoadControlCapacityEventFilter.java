/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr.api.filter;

import com.enernoc.open.oadr2.model.v20b.ei.EiEventSignal;
import com.enernoc.open.oadr2.model.v20b.ei.SignalTypeEnumeratedType;

/**
 *
 * @author ancla
 */
public class LoadControlCapacityEventFilter implements OpenADREventFilter {

    @Override
    public boolean accept(EiEventSignal p) {
        if(p.getSignalName().equals("load_control") && p.getSignalType() == SignalTypeEnumeratedType.X_LOAD_CONTROL_CAPACITY)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
}
