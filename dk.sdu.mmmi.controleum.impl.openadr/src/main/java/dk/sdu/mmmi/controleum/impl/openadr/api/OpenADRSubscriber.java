/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.openadr.api;

import dk.sdu.mmmi.controleum.impl.openadr.api.filter.OpenADREventFilter;

/**
 *
 * @author ancla
 */
public interface OpenADRSubscriber {
    public void notify(OpenADRAction packet);
    
    public OpenADREventFilter getFilter();
}
