package dk.sdu.mmmi.controleum.api.electricityprices;

import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author jemr & jcs
 */
public interface EnergyPriceService {

    /**
     * Returns an array with timestamps and values, this method makes it
     * possible to poll all the needed data in one select instead of polling
     * each time
     *
     * @param area
     * @param startDateTime
     * @param endDateTime
     * @return a multi dimensional array of objects with a timestamp and value
     * in euro
     */
    Map<Date, Double> getPrices(EnergyPriceDatabaseArea area, Date startDateTime, Date endDateTime) throws EnergyPriceDatabaseException;
}
