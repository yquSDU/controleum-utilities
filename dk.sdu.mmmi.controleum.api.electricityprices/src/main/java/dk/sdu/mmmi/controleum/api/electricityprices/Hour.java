package dk.sdu.mmmi.controleum.api.electricityprices;

/**
 *
 * @author jcs
 */
public enum Hour {

    Slot_00, Slot_01, Slot_02, Slot_03, Slot_04, Slot_05, Slot_06, Slot_07,
    Slot_08, Slot_09, Slot_10, Slot_11, Slot_12, Slot_13, Slot_14, Slot_15,
    Slot_16, Slot_17, Slot_18, Slot_19, Slot_20, Slot_21, Slot_22, Slot_23
}
