package dk.sdu.mmmi.controleum.api.electricityprices;

/**
 *
 * @author jcs
 */
public class EnergyPriceDatabaseException extends Exception {

    public EnergyPriceDatabaseException() {
        super();
    }

    public EnergyPriceDatabaseException(Throwable cause) {
        super(cause);
    }

    public EnergyPriceDatabaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public EnergyPriceDatabaseException(String message) {
        super(message);
    }
}
