/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public abstract class ControleumValueInput<T> extends HazelcastIO<T> {

    public ControleumValueInput(UUID inputId, String readAddress, String map) {
        super(inputId, readAddress, map);
    }
    
}
