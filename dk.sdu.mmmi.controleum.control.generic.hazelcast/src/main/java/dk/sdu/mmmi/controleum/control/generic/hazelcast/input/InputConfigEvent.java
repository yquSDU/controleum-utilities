/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast.input;

import dk.sdu.mmmi.controleum.api.control.generic.BasicEvent;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public class InputConfigEvent extends BasicEvent<InputConfig> {
    public InputConfigEvent(UUID id, InputConfig cfg)
    {
        super(id, cfg);
    }

}
