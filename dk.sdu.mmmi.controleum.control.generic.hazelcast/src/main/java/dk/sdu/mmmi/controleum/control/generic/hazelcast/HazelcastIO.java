/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast;

;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.MapEvent;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.EntryListener;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import dk.sdu.mmmi.controleum.api.moea.Element;
import java.net.URL;
import java.util.UUID;

/**
 *
 * @author ancla
 */


public abstract class HazelcastIO<T> implements Element, EntryListener<String, T> {

    protected final IMap distMap;
    private final UUID id;
    private T value;
    static ClientConfig config = new ClientConfig();
    static HazelcastInstance client = HazelcastClient.newHazelcastClient(config);
    private final String mapAddress;
    private final String entryId;
    public HazelcastIO(UUID id, String mapAddress, String map) {
        distMap = client.getMap(map);
        entryId = distMap.addEntryListener(this, true);
        this.id = id;
        this.mapAddress = mapAddress;

        readInitial();
    }

    public UUID getId() {
        return id;
    }

    //For output
    public void setMapValue(T value) {
        this.value = value;
        //System.out.println("Setting output on " + id + " in " + distMap.getName());
        distMap.put(mapAddress, value);
        notifyValueSet();
    }

    private final void readInitial() {
        try {
            value = (T) distMap.get(mapAddress);
        } catch (Throwable ex) {
            System.out.println("Warning: Unable to parse object from map. Null value or malformed data.");
            ex.printStackTrace();
        }
    }

    //For input
    public T getValue() {
        /*        Future<T> future = distMap.getAsync(id);
         this.value = future.get();*/
        return value;
    }

    public abstract void notifyValueSet();

    public abstract void notifyValueUpdated();

    @Override
    public URL getHtmlHelp() {
        return null;
    }

    @Override
    public final String toString() {
        if (value != null) {
            return String.format("%s =\t%s", getName(), value);
        } else {
            return String.format("%s", getName());
        }

    }

    @Override
    public void entryAdded(EntryEvent<String, T> ee) {
        checkUpdate(ee);
    }

    @Override
    public void entryRemoved(EntryEvent<String, T> ee) {
        checkUpdate(ee);
    }

    @Override
    public void entryUpdated(EntryEvent<String, T> ee) {
        checkUpdate(ee);
    }

    private void checkUpdate(EntryEvent<String, T> ee) {
        //System.out.println("Checking update..");
        if (ee.getKey().equals(this.mapAddress)) {
            //System.out.println("Fetching update for " + id + " on " + distMap.getName());
            this.value = ee.getValue();
            notifyValueUpdated();
        }
    }

    @Override
    public void entryEvicted(EntryEvent<String, T> ee) {
        checkUpdate(ee);
    }

    @Override
    public void mapEvicted(MapEvent me) {
    }

    @Override
    public void mapCleared(MapEvent me) {

    }

    protected void hazDispose() {
        distMap.removeEntryListener(entryId);
    }
}
