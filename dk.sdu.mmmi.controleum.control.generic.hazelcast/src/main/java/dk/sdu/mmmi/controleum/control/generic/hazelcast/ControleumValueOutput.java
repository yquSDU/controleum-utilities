/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public abstract class ControleumValueOutput extends HazelcastIO<ControleumValueList> {

    public ControleumValueOutput(UUID outputId, String writeAddress, String map) {
        super(outputId, writeAddress, map);
    }
    
}
