/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast.input;

/**
 *
 * @author ancla
 */
public interface InputConfigEventListener {
    public void notifyAdded(InputConfigEvent e);
    public void notifyModified(InputConfigEvent e);
    public void notifyDeleted(InputConfigEvent e);
}
