/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast.input;

import com.decouplink.Disposable;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.control.generic.hazelcast.ControleumValueInput;
import dk.sdu.mmmi.controleum.api.control.generic.ValueEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;


/**
 *
 * @author ancla
 */
public class InputImpl<T> extends ControleumValueInput<T> implements Disposable {

    private InputConfig cfg;
    private final ControleumContext cd;
    
    public InputImpl(ControleumContext cd, InputConfig cfg) {
        super(cfg.getId(), cfg.getReadAddress(), cfg.getMapName());
        this.cfg = cfg;
        this.cd = cd;
    }

    @Override
    public String getID() {
        return getId().toString();
    }

    /*
    @Deprecated Use InputConfigManager.setConfig() instead.
    */
    @Deprecated
    @Override
    public void setName(String name) {
        this.cfg.setName(name);
    }

    @Override
    public String getName() {
        return this.cfg.getName();
    }

    public void setConfig(InputConfig cfg) {
        this.cfg = cfg;
    }    

    @Override
    public void notifyValueSet() {
        
    }

    @Override
    public void notifyValueUpdated() {
        ValueEvent e = new ValueEvent(getId(), getValue());
        for(InputValueListener l : context(cd).all(InputValueListener.class))
        {
            l.notify(e);
        }
    }

    @Override
    public void dispose() {
        this.hazDispose();
    }
    
}
