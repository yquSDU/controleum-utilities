/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast.input;

import dk.sdu.mmmi.controleum.api.control.generic.BasicConfig;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public class InputConfig extends BasicConfig {
    public InputConfig()
    {
        super("Standard Input");
    }
    
    private String readAddress;

    /**
     * @return the readAddress
     */
    public String getReadAddress() {
        return readAddress;
    }

    /**
     * @param readAddress the readAddress to set
     */
    public void setReadAddress(String readAddress) {
        this.readAddress = readAddress;
    }
    
}
