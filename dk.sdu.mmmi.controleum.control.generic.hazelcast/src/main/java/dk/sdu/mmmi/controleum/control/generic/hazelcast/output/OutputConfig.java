/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast.output;

import dk.sdu.mmmi.controleum.api.control.generic.BasicConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public class OutputConfig extends BasicConfig {

    private List<UUID> issueIds;
    private String writeAddress;

    public OutputConfig() {
        super(UUID.randomUUID(), "Noname");
        this.issueIds = new ArrayList<>();
    }

    public OutputConfig(String name) {
        super(UUID.randomUUID(), name);
        this.issueIds = new ArrayList<>();
    }

    public OutputConfig(UUID id, String name, List<UUID> issueIds) {
        super(id, name);
        this.issueIds = issueIds;
    }
    
    public OutputConfig(UUID id, String name, String mapName, List<UUID> issueIds)
    {
        super(id,name,mapName);
        this.issueIds = issueIds;
    }

    /**
     * @return the issueIds
     */
    public List<UUID> getIssueIds() {
        return issueIds;
    }

    /**
     * @param issueIds the issueIds to set
     */
    public void setIssueIds(List<UUID> issueIds) {
        this.issueIds = issueIds;
    }
    
    public void setWriteAddress(String writeAddress)
    {
        this.writeAddress = writeAddress;
    }
    
    public String getWriteAddress()
    {
        return writeAddress;
    }
}
