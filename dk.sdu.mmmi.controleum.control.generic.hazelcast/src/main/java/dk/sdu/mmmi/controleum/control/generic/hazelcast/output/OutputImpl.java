/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.hazelcast.output;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import dk.sdu.mmmi.controleum.api.control.generic.ValueEvent;
import dk.sdu.mmmi.controleum.control.generic.hazelcast.ControleumValueOutput;
import dk.sdu.mmmi.controleum.api.moea.Element;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssueValueListener;

/**
 *
 * @author ancla
 */
public class OutputImpl extends ControleumValueOutput implements GenericIssueValueListener, Element {

    private OutputConfig cfg;
    private final ControleumContext cd;
    private ControleumValueList value;

    public OutputImpl(ControleumContext cd, OutputConfig cfg) {
        super(cfg.getId(), cfg.getWriteAddress(), cfg.getMapName());
        this.cfg = cfg;
        this.cd = cd;
    }


    protected void setConfig(OutputConfig cfg) {
        this.cfg = cfg;
    }

    //TODO Not able to generate aggregated outputs for now.
    @Override
    public synchronized void notify(ValueEvent event) {
        if (cfg.getIssueIds().contains(event.getId())) {
            setMapValue((ControleumValueList)event.getValue());
        }
    }

    @Deprecated
    @Override
    public void setName(String name) {
        throw new UnsupportedOperationException("Not supported. Never will be. CFG based config through manager."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        return this.cfg.getName();
    }

    @Override
    public String getID() {
        return getId().toString();
    }

    @Override
    public void notifyValueSet() {
        ValueEvent e = new ValueEvent(getId(), getValue());
        for (OutputValueListener l : context(cd).all(OutputValueListener.class)) {
            l.notify(e);
        }
    }

    @Override
    public void notifyValueUpdated() {

    }
/*
    @Override
    public void doSetup() {
        value = new ControleumValueListImpl(0.0);
    }

    @Override
    public boolean isSubsystemAgent() {
        return false;
    }
    */
}
