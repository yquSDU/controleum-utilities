package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class MolSqrMeter implements Unit {

    private final BigDecimal value;

    public MolSqrMeter(BigDecimal value) {
        this.value = value;
    }

    public MolSqrMeter(Double value) {
        this(valueOf(value));
    }

    public MolSqrMeter(double value) {
        this(valueOf(value));
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    @Override
    public double roundedValue() {
        return value.setScale(2, RoundingMode.UP).doubleValue();
    }

    @Override
    public String toString() {
        return String.format("%.3f [mol m^-2]", value());
    }

    public MolSqrMeter subtract(MolSqrMeter added) {
        return new MolSqrMeter(this.value.subtract(added.value));
    }

    public MolSqrMeter add(MolSqrMeter added) {
        return new MolSqrMeter(this.value.add(added.value));
    }

    public MolSqrMeter multiply(double d) {
        return new MolSqrMeter(value.multiply(valueOf(d)));
    }
}
