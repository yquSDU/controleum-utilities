package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;
import java.util.Objects;

/**
 *
 * @author jcs
 */
public class MMolSqrMeter implements Unit {

    private BigDecimal value;

    public MMolSqrMeter(BigDecimal value) {
        this.value = value;
    }

    public MMolSqrMeter(Double value) {
        this(valueOf(value));
    }

    public MMolSqrMeter(double value) {
        this(valueOf(value));
    }

    @Override
    public String toString() {
        return String.format("%.1f [mmol m^-2]", value());
    }

    public MMolSqrMeter add(MMolSqrMeter added) {
        return new MMolSqrMeter(this.value.add(added.value));
    }

    public MMolSqrMeter subtract(MMolSqrMeter s) {
        return new MMolSqrMeter(this.value.subtract(s.value));
    }

    public MMolSqrMeter divide(double factor) {
        return new MMolSqrMeter(this.value.divide(valueOf(factor)));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MMolSqrMeter other = (MMolSqrMeter) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    public MMolSqrMeter multiply(double factor) {
        return new MMolSqrMeter(this.value.multiply(valueOf(factor)));
    }
}
