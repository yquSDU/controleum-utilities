package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Duration;
import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class KiloWatt implements Unit {

    private final BigDecimal watt;

    public KiloWatt(BigDecimal watt) {
        this.watt = watt;
    }

    public KiloWatt(Double watt) {
        this(valueOf(watt));
    }

    public KiloWatt(double watt) {
        this(valueOf(watt));
    }

    @Override
    public double value() {
        return watt.doubleValue();
    }

    @Override
    public double roundedValue() {
        return watt.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return watt;
    }

    @Override
    public String toString() {
        return String.format("%.1f [KW]", value());
    }

    public KiloWattHour times(Duration numOfHours) {
        return new KiloWattHour(watt.multiply(valueOf(numOfHours.toHours())));
    }
}
