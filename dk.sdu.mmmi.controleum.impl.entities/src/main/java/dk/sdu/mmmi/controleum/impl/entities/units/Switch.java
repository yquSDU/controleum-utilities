package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;

/**
 * @author mrj
 */
public enum Switch implements Unit {

    ON("1", 1), OFF("0", 0), NA("?", -1);
    private final int satusValue;
    private final String statusText;

    private Switch(String txt, int satus) {
        this.statusText = txt;
        this.satusValue = satus;
    }

    public boolean isOn() {
        return this == ON;
    }

    public boolean isOff() {
        return this == OFF;
    }

    public boolean isNotAvailable() {
        return this == NA;
    }

    @Override
    public double value() {
        return BigDecimal.valueOf(satusValue).doubleValue();
    }

    @Override
    public double roundedValue() {
        return value();
    }

    @Override
    public BigDecimal bigDecimal() {
        return BigDecimal.valueOf(satusValue);
    }

    @Override
    public String toString() {
        return String.format("%s", statusText);
    }
}
