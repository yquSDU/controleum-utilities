package dk.sdu.mmmi.controleum.impl.entities.config;

/**
 *
 * @author jcs
 */
public final class LightForecastConfig {

    private boolean isActive = false;
    private String location = "knudjepsen"; //TODO //qqq origin: "sdu";

    //
    // Setters
    //
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    //
    // Getters
    //
    public boolean isActive() {
        return isActive;
    }

    public String getLocation() {
        return this.location;
    }
}
