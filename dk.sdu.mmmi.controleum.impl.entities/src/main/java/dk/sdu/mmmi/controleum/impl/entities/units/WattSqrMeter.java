package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class WattSqrMeter implements Unit {

    private final BigDecimal value;

    public WattSqrMeter(BigDecimal value) {
        this.value = value;
    }

    public WattSqrMeter(Double wattSqr) {
        this(valueOf(wattSqr));
    }

    public WattSqrMeter(double wattSqr) {
        this(valueOf(wattSqr));
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%.1f [W m^-2]", value());
    }

    public WattSqrMeter times(Percent pct) {
        BigDecimal f = valueOf(pct.asFactor());
        return new WattSqrMeter(value.multiply(f));
    }

    public Watt times(SqrMeter size) {
        BigDecimal s = size.bigDecimal();
        return new Watt(this.value.multiply(s));
    }

    /**
     * Watt PAR m^-2 spectra into umol m^-2 s^-1
     *
     * @return Light level in PAR spectra
     */
    public UMolSqrtMeterSecond toUMolPAR() {
        return new UMolSqrtMeterSecond(value.multiply(valueOf(2.3)));
    }

}
