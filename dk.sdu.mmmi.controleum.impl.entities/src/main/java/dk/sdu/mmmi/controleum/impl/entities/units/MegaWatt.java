package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Duration;
import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class MegaWatt implements Unit {

    private final BigDecimal watt;

    public MegaWatt(BigDecimal watt) {
        this.watt = watt;
    }

    public MegaWatt(Double watt) {
        this(valueOf(watt));
    }

    public MegaWatt(double watt) {
        this(valueOf(watt));
    }

    @Override
    public double value() {
        return watt.doubleValue();
    }

    @Override
    public double roundedValue() {
        return watt.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return watt;
    }

    @Override
    public String toString() {
        return String.format("%.1f [MW]", value());
    }

    public MegaWattHour times(Duration numOfHours) {
        return new MegaWattHour(watt.multiply(valueOf(numOfHours.toHours())));
    }
}
