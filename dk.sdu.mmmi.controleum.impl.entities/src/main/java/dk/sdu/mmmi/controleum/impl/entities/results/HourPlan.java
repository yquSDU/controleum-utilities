package dk.sdu.mmmi.controleum.impl.entities.results;



import dk.sdu.mmmi.controleum.common.units.Duration;
import java.util.*;

/**
 * @author mrj
 */
public class HourPlan<T> {

    private final static Calendar C = Calendar.getInstance();
    private final int startHour;
    private final Date start, end;
    private final List<T> list;
    private final long HOUR_MS = 1000 * 60 * 60;

    public HourPlan(Date start, Date end, T defaultElement) {

        // Find start time.
        C.setTime(start);
        C.set(Calendar.MILLISECOND, 0);
        C.set(Calendar.SECOND, 0);
        C.set(Calendar.MINUTE, 0);
        this.start = C.getTime();

        // Find start hour.
        this.startHour = C.get(Calendar.HOUR_OF_DAY);

        // Find end hour.
        C.setTime(end);
        this.end = C.getTime();
        int endHour = C.get(Calendar.HOUR_OF_DAY);

        // Find size.
        int size = endHour - startHour + 1;

        this.list = new ArrayList<T>(size);

        // Fill with default values.
        for (int i = 0; i < size; i++) {
            list.add(defaultElement);
        }
    }

    public HourPlan(HourPlan<T> copyFrom) {
        this.startHour = copyFrom.startHour;
        this.start = copyFrom.start;
        this.end = copyFrom.end;
        this.list = new ArrayList<T>(copyFrom.list);
    }

    //
    // Accessors.
    //
    public int size() {
        return list.size();
    }

    public List<T> getList() {
        return Collections.unmodifiableList(list);
    }

    /**
     * @param idx Index of light plan
     * @return Hour of day for a given index in the light plan
     */
    public int getElementHourOfDay(int idx) {
        return (startHour + idx) % 24;
    }

    public Date getStart() {
        return start;
    }

    /**
     * @param idx Index of light plan
     * @return Start hour for the given light plan index.
     */
    public Date getElementStart(int idx) {
        return new Date(start.getTime() + idx * HOUR_MS);
    }

    /**
     * @param idx Index of light plan
     * @return End hour for the given light plan index.
     */
    public Date getElementEnd(int idx) {
        return new Date(start.getTime() + (idx + 1) * HOUR_MS);
    }

    public T getElement(int idx) {
        return list.get(idx);
    }

    public T getElementForHour(int hour) {
        return getElement(hour - startHour);
    }

    public boolean hasHour(int hour) {
        int idx = hour - startHour;
        return idx >= 0 && idx < size();
    }

    public Duration getPlanDuration() {
        return new Duration(new Long(size() * 1000 * 60));      //TODO //qqq size() * 1000 * 60 -> new Long(size() * 1000 * 60)
    }

    //
    // Modifiers.
    //
    public void setElement(int idx, T e) {
        this.list.set(idx, e);
    }

    public void setElementForHour(int hour, T e) {
        setElement(hour - startHour, e);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HourPlan<T> other = (HourPlan<T>) obj;
        if (this.start != other.start && (this.start == null || !this.start.equals(other.start))) {
            return false;
        }
        if (this.list != other.list && (this.list == null || !this.list.equals(other.list))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.start != null ? this.start.hashCode() : 0);
        hash = 97 * hash + (this.list != null ? this.list.hashCode() : 0);
        return hash;
    }
}
