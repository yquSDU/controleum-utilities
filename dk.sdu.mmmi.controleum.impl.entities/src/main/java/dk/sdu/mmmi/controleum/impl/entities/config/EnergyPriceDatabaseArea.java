package dk.sdu.mmmi.controleum.impl.entities.config;



/**
 *
 * @author jemr
 */
public enum EnergyPriceDatabaseArea {
    SYS("sys"),
    DK_EAST("dkeast"),
    DK_WEST("dkwest");

    private final String dbName;
    EnergyPriceDatabaseArea(String name) {
	dbName = name;
    }

    public String getDBName() {
	return dbName;
    }

}
