package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class SqrMeter implements Unit {

    private final BigDecimal value;

    public SqrMeter(BigDecimal value) {
        this.value = value;
    }

    public SqrMeter(Double value) {
        this(valueOf(value));
    }

    public SqrMeter(double value) {
        this(valueOf(value));
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public String toString() {
        return String.format("%.1f [m^2]", value());
    }
}
