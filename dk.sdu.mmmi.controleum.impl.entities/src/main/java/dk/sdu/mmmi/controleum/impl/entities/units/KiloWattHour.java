package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class KiloWattHour implements Unit {

    private BigDecimal value;

    public KiloWattHour(double value) {
        this(valueOf(value));
    }

    public KiloWattHour(Double value) {
        this(valueOf(value));
    }

    public KiloWattHour(BigDecimal kiloWattHour) {
        this.value = kiloWattHour;
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%.1f [KWh]", value());
    }

    public KiloWattHour plus(KiloWattHour KWh) {
        this.value = this.value.add(KWh.value);
        return new KiloWattHour(this.value);
    }

    public MegaWattHour toMegaWattHour() {
        BigDecimal exp = BigDecimal.valueOf(Math.pow(10, -3));
        return new MegaWattHour(this.value.multiply(exp));
    }
}
