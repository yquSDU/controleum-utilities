package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 * @author mrj
 */
public class Energy implements Unit {

    private final BigDecimal value;

    public Energy(BigDecimal value) {
        this.value = value;
    }

    public Energy(Double value) {
        this(valueOf(value));
    }

    public Energy(double value) {
        this(valueOf(value));
    }

    @Override
    public String toString() {
        return String.format("%.1f [Energy]", value());
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

}
