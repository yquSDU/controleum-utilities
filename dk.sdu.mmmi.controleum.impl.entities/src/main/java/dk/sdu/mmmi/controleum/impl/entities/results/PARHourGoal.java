/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dk.sdu.mmmi.controleum.impl.entities.results;

import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author yqu
 */
public class PARHourGoal {
    
    private List<Sample<WattSqrMeter>> hourGoal;
    
    public PARHourGoal(){
        hourGoal = new ArrayList<>();
    }
    
    public PARHourGoal(List<Sample<WattSqrMeter>> goal){
        hourGoal = goal;
    }
    
    public List<Sample<WattSqrMeter>> get() {
        return hourGoal;
    }
}
