package dk.sdu.mmmi.controleum.impl.entities.config;

import dk.sdu.mmmi.controleum.common.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;

/**
 *
 * @author jcs
 */
public class LightPlanConfig {

    private final Duration timeslotDuration;
    private final boolean elPriceCacheEnabled;

    public static class Builder {

        private Duration timeslotDuration = new Duration(DateUtil.HOUR_IN_MS);
        private boolean elPriceCacheEnabled = true;

        public Builder duration(long ms) {
            this.timeslotDuration = new Duration(ms);
            return this;
        }

        public Builder enableElPriceCache(boolean isEnabled) {
            this.elPriceCacheEnabled = isEnabled;
            return this;
        }

        public LightPlanConfig build() {
            return new LightPlanConfig(this);   //qqq ??? what is the meaning?
        }
    }

    private LightPlanConfig(Builder b) {
        this.timeslotDuration = b.timeslotDuration;
        this.elPriceCacheEnabled = b.elPriceCacheEnabled;
    }

    public Duration getTimeSlotDuration() {
        return timeslotDuration;
    }

    public boolean isElPriceCacheEnabled() {
        return elPriceCacheEnabled;
    }

}
