package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Objects;

public final class Money implements Unit {

    /**
     * The money amount. Never null.
     *
     * @serial
     */
    private BigDecimal fAmount;
    /**
     * The currency of the money, such as US Dollars or Euros. Never null.
     *
     * @serial
     */
    private final Currency fCurrency;

    /**
     * Constructor taking the money amount and currency.
     *
     * <P>
     * The rounding style takes a default value.
     *
     * @param aAmount is required, can be positive or negative.
     * @param aCurrency is required.
     */
    public Money(BigDecimal aAmount, Currency aCurrency) {
        this.fAmount = aAmount;
        this.fCurrency = aCurrency;
    }

    /**
     * Constructor taking the money amount and currency.
     *
     * <P>
     * The rounding style takes a default value.
     *
     * @param aAmount is required, can be positive or negative.
     * @param aCurrency the ISO 4217 code of the currency
     */
    public Money(BigDecimal aAmount, String aCurrency) {
        this(aAmount, Currency.getInstance(aCurrency));
    }

    /**
     * Return the amount passed to the constructor.
     */
    @Override
    public double value() {
        return fAmount.doubleValue();
    }

    @Override
    public double roundedValue() {
        return fAmount.setScale(0, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return fAmount;
    }

    /**
     * Return the currency passed to the constructor, or the default currency.
     */
    public Currency getCurrency() {
        return fCurrency;
    }

    public Money times(double money) {
        return times(BigDecimal.valueOf(money));
    }

    public Money times(Money money) {
        return times(money.fAmount);
    }

    public Money times(BigDecimal money) {
        BigDecimal newAmount = fAmount.multiply(money);
        return new Money(newAmount, fCurrency);
    }

    public Money plus(Money value) {
        BigDecimal newAmount = this.fAmount.add(value.fAmount);
        return new Money(newAmount, fCurrency);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(fAmount.toPlainString());
        sb.append("[");
        sb.append(fCurrency);
        sb.append("]");
        return sb.toString();
    }

    public String formattedString() {
        return java.text.NumberFormat.getCurrencyInstance().format(this.value());
    }

    /**
     * Like {@link BigDecimal#equals(java.lang.Object)}, this <tt>equals</tt>
     * method is also sensitive to scale.
     *
     * For example, <tt>10</tt> is <em>not</em> equal to <tt>10.00</tt> The
     * {@link #eq(Money)} method, on the other hand, is <em>not</em> sensitive
     * to scale.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.fAmount);
        hash = 47 * hash + Objects.hashCode(this.fCurrency);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Money other = (Money) obj;
        if (!Objects.equals(this.fAmount, other.fAmount)) {
            return false;
        }
        if (!Objects.equals(this.fCurrency, other.fCurrency)) {
            return false;
        }
        return true;
    }

}
