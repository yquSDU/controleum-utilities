package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * @author mrj
 */
public class Percent implements Unit {

    private final BigDecimal percent;

    public Percent(BigDecimal percent) {
        this.percent = percent;
    }

    public Percent(Double percent) {
        this(valueOf(percent));
    }

    public Percent(double percent) {
        this(valueOf(percent));
    }

    public double asPercent() {
        return percent.doubleValue();
    }

    public double asFactor() {
        return percent.divide(valueOf(100.0)).doubleValue();
    }

    public static Percent fromPercent(double percent) {
        return new Percent(percent);
    }

    public static Percent fromFactor(double factor) {
        return new Percent(factor * 100.0);
    }

    @Override
    public String toString() {
        return String.format("%.1f [pct]", asPercent());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.percent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Percent other = (Percent) obj;
        if (!Objects.equals(this.percent, other.percent)) {
            return false;
        }
        return true;
    }

    @Override
    public double value() {
        return percent.doubleValue();
    }

    @Override
    public double roundedValue() {
        return percent.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return percent;
    }

}
