package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * @author mrj
 */
public class Celcius implements Unit {

    private final BigDecimal celcius;

    public Celcius(BigDecimal celcius) {
        this.celcius = celcius;
    }

    public Celcius(Double celcius) {
        this(valueOf(celcius));
    }
    
    public Celcius(double celcius) {
        this(valueOf(celcius));
    }

    @Override
    public double value() {
        return celcius.doubleValue();
    }

    @Override
    public double roundedValue() {
        return celcius.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return celcius;
    }

    public Celcius subtract(Celcius s) {
        return new Celcius(celcius.subtract(s.bigDecimal()));
    }

    @Override
    public String toString() {
        return String.format("%.1f [C]", value());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.celcius);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Celcius other = (Celcius) obj;
        if (!Objects.equals(this.celcius, other.celcius)) {
            return false;
        }
        return true;
    }
}
