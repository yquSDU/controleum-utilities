package dk.sdu.mmmi.controleum.impl.entities.config;

/**
 *
 * @author jcs
 */
public enum EnergyPriceDatabaseType {

    NORD_POOL, ENERGI_DK
}
