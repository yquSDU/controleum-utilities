package dk.sdu.mmmi.controleum.impl.entities.units;

import java.math.BigDecimal;
import java.util.Date;
import dk.sdu.mmmi.controleum.common.units.Unit;

/**
 *
 * @author corfixen
 */
public class TimeStamp implements Unit {

    private final long ms;

    public TimeStamp(Long ms) {
        this.ms = ms;
    }

    public TimeStamp(long ms) {
        this.ms = ms;
    }

    public TimeStamp(Date date) {
        this.ms = date.getTime();
    }

    public Date toDate() {
        return new Date(ms);
    }

    @Override
    public double value() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double roundedValue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal bigDecimal() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public long toMS() {
        return ms;
    }

    @Override
    public String toString() {
        return toDate().toString();
    }

}
