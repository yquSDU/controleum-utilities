package dk.sdu.mmmi.controleum.impl.entities.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 *
 * @author mrj
 */
public class DateUtil {

    public static final long MIN_IN_MS = 1 * 60 * 1000L;
    public static final long FIVE_MINS_IN_MS = 5 * 60 * 1000L;
    public static final long TEEN_MINS_IN_MS = 10 * 60 * 1000L;
    public static final long FIFTEEN_MINS_IN_MS = 15 * 60 * 1000L;
    public static final long TWENTY_MINS_IN_MS = 20 * 60 * 1000L;
    public static final long TWENTYFIVE_MINS_IN_MS = 25 * 60 * 1000L;
    public static final long THIRDTY_MINS_IN_MS = 30 * 60 * 1000L;
    public static final long THIRDTYFIVE_IN_MS = 35 * 60 * 1000L;
    public static final long FOURTY_MINS_IN_MS = 40 * 60 * 1000L;
    public static final long FOURTYFIVE_MINS_IN_MS = 45 * 60 * 1000L;
    public static final long FIFTY_MINS_IN_MS = 50 * 60 * 1000L;
    public static final long HOUR_IN_MS = 1000 * 60 * 60;
    public static final long HOUR_IN_S = 60 * 60;
    public static final long S_IN_MS = 1000;
    public final static int WINDOW_SIZE = 3;    //qqq PAR sum achieved within the sliding window. that is how many days of inputs and optimzation are considered. origin is 5.
    public final static long DAY_IN_MS = HOUR_IN_MS * 24L;  //qqq add
    public final static long TIMESTAMP_THREATHOLD = (long) Math.pow(10, 11);
    public final static int DAY_IN_HOUR = 24;

    public static Date startOfHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    public static Date endOfHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        return calendar.getTime();
    }

    public static Date startOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    public static Date endOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        return calendar.getTime();
    }

    public static Date startOfTwoDaysBefore(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, -2);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    public static Date startOfXDaysBefore(Date date, int daysBefore) {      //qqq copy from v1
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, -daysBefore);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    public static Date endOfXDaysAfter(Date date, int daysAfter) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, daysAfter);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        return calendar.getTime();
    }

    public static Date startofXHoursAfter(Date date, int hoursAfter) {    //TODO //qqq add for responding Production Twin request

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hoursAfter - 1);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }
        
    public static Date endofXHoursAfter(Date date, int hoursAfter) {    //TODO //qqq add for responding Production Twin request

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hoursAfter - 1);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        return calendar.getTime();
    }

    public static int hourOfDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static Date dateOfIndex(int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    public static Date ealiest(Date a, Date b) {
        return a.before(b) ? a : b;
    }

    public static Date latest(Date a, Date b) {
        return a.after(b) ? a : b;
    }

    public static int hourOfMillis(long hourMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(hourMillis));
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static Date timeWeekBefore(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        return calendar.getTime();
    }

    public static Date endOfDayWeekBefore(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        return calendar.getTime();
    }

    public static long hourDifference(Date fr, Date to) {       //qqq add

        long difference = to.getTime() - fr.getTime();
        long hourDiff = (difference / HOUR_IN_MS) + 1;

        return hourDiff;
    }


    public static List<Date> datePeriodList(Date start, Date end) {       //qqq add
        long startTime = start.getTime();
        long endTime = end.getTime();
        long DAY = 60 * 60 * 1000;
        TimeZone itemTimeZone = TimeZone.getTimeZone("GMT+1");

        List<Long> timeList = new ArrayList<>();
        List<Date> dateList = new ArrayList<>();

        long ltime = startTime;
        timeList.add(ltime);
        dateList.add(new Date(ltime));

        while (ltime < endTime) {
            long last = timeList.get(timeList.size() - 1);
            long beforeStartP = itemTimeZone.getOffset(last);

            ltime = last + DAY;

            long nowStartP = itemTimeZone.getOffset(ltime);

            ltime = ltime + (nowStartP - beforeStartP);

            timeList.add(ltime);
            Date d = new Date(ltime);
            dateList.add(d);
        }

        return dateList;
    }

    public static int weekNumInYear(Date d) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        int weekNum = cal.get(Calendar.WEEK_OF_YEAR);

        return weekNum;
    }

    

}
