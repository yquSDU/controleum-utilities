package dk.sdu.mmmi.controleum.impl.entities.results;

import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.WattSqrMeter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Outdoor light forecast.
 *
 * @author mrj
 */
public final class OutdoorLightForecast {

    private List<Sample<WattSqrMeter>> forecast;

    public OutdoorLightForecast() {
        forecast = new ArrayList<>();
    }

    public OutdoorLightForecast(List<Sample<WattSqrMeter>> forecast) {
        this.forecast = forecast;
    }

    public List<Sample<WattSqrMeter>> get() {
        return forecast;
    }

    /**
     * @param from Start of forecast.
     * @param to End of forecast.
     * @return Forecast for the period [from;to].
     */
    public OutdoorLightForecast getPeriod(Date from, Date to) {

        List<Sample<WattSqrMeter>> r = new ArrayList<>();

        for (Sample<WattSqrMeter> sample : forecast) {

            Date sampleTime = sample.getTimestamp();

            if ((sampleTime.equals(from) || sampleTime.after(from))
                    && (sampleTime.equals(to) || sampleTime.before(to))) {
                r.add(sample);
            }
        }
        return new OutdoorLightForecast(r);
    }

    @Override
    public String toString() {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Sample<WattSqrMeter> t : forecast) {
            sb.append(df.format(t.getTimestamp()));
            sb.append("=");
            WattSqrMeter light = t.getSample();
            sb.append(light);
            sb.append(",    ");
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.forecast);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OutdoorLightForecast other = (OutdoorLightForecast) obj;
        if (!Objects.equals(this.forecast, other.forecast)) {
            return false;
        }
        return true;
    }
}
