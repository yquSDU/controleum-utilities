package dk.sdu.mmmi.controleum.impl.entities.config;

/**
 *
 * @author jcs
 */
public class ElPriceForecastConfig {

    private boolean isActive = false;
    private int forecastPeriod = 1000 * 60 * 60 * 24;
    private EnergyPriceDatabaseArea area = EnergyPriceDatabaseArea.DK_WEST;
    private String currency = "EUR";

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public void setForecastPeriod(int forecastPeriod) {
        this.forecastPeriod = forecastPeriod;
    }

    public void setArea(EnergyPriceDatabaseArea area) {
        this.area = area;
    }

    /**
     *
     * @param currency The ISO 4217 code of the currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public EnergyPriceDatabaseArea getElPriceArea() {
        return area;
    }

    public String getCurrency() {
        return currency;
    }

    public long getForecastPeriod() {
        return forecastPeriod;
    }
}
