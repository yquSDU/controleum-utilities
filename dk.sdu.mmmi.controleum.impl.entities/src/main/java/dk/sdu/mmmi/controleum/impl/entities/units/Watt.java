package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class Watt implements Unit {

    private final BigDecimal value;

    public Watt(BigDecimal watt) {
        this.value = watt;
    }

    public Watt(Double watt) {
        this.value = valueOf(watt);
    }

    public Watt(double watt) {
        this.value = valueOf(watt);
    }

    @Override
    public String toString() {
        return String.format("%.1f [W]", value());
    }

    public MegaWatt toMegaWatt() {
        BigDecimal exp = BigDecimal.valueOf(Math.pow(10, -6));
        return new MegaWatt(this.value.multiply(exp));
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

}
