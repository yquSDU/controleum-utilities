package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Duration;
import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;
import static java.math.RoundingMode.HALF_UP;
import java.util.Objects;

/**
 * @author mrj
 */
public class UMolSqrtMeterSecond implements Unit {

    private final BigDecimal value;

    public UMolSqrtMeterSecond(BigDecimal umol) {
        this.value = umol;
    }

    public UMolSqrtMeterSecond(Double umol) {
        this(valueOf(umol));
    }

    public UMolSqrtMeterSecond(double umol) {
        this(valueOf(umol));
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    /**
     * Conversion of light in PAR spectra.
     *
     * @return PAR light in W/m2
     */
    public WattSqrMeter fromPARtoPAR() {
        return new WattSqrMeter(value.divide(valueOf(4.6), HALF_UP));
    }

    /**
     * Convert PAR light value to Sun spectra
     *
     * @return
     */
    public WattSqrMeter fromPARtoSun() {
        return new WattSqrMeter(value.divide(valueOf(2.3), HALF_UP));
    }

    public UMolSqrMeter times(Duration duration) {
        BigDecimal secs = BigDecimal.valueOf(duration.toSeconds());
        return new UMolSqrMeter(value.multiply(secs));
    }

    public UMolSqrtMeterSecond times(Percent p) {
        BigDecimal f = valueOf(p.asFactor());
        return new UMolSqrtMeterSecond(value.multiply(f));
    }

    public UMolSqrtMeterSecond divide(UMolSqrtMeterSecond photoWithoutScreens) {
        BigDecimal p = photoWithoutScreens.bigDecimal();
        return new UMolSqrtMeterSecond(value.divide(p, HALF_UP));
    }

    public UMolSqrtMeterSecond divide(double factor) {
        return new UMolSqrtMeterSecond(value.divide(valueOf(factor), HALF_UP));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UMolSqrtMeterSecond other = (UMolSqrtMeterSecond) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("%.1f [umol m^-2 s^-1]", value());
    }

    public UMolSqrtMeterSecond add(UMolSqrtMeterSecond v) {
        BigDecimal val = v.bigDecimal();
        return new UMolSqrtMeterSecond(value.add(val));
    }

    public UMolSqrtMeterSecond subtract(UMolSqrtMeterSecond v) {
        BigDecimal val = v.bigDecimal();
        return new UMolSqrtMeterSecond(value.subtract(val));
    }
}
