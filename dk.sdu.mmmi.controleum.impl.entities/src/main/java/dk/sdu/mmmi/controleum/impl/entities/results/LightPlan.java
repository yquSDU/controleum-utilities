package dk.sdu.mmmi.controleum.impl.entities.results;

import dk.sdu.mmmi.controleum.common.units.Duration;
import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfHour;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.hourOfDate;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.startOfHour;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.hourDifference;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author jcs
 */
public class LightPlan {

    private final int startHour;
    private final Date start, end;
    private final List<Sample<Switch>> list;
    private final Duration lightInterval;
    

    public LightPlan(Date start, Date end, Duration lightSlotInterval) {

        // Find start time.
        this.start = startOfHour(start);
        this.startHour = hourOfDate(start);

        // Find end hour.
        this.end = endOfHour(end);

        // Find size.
        //qqq calculate LightPlan size  //qqq origin: int size = hourOfDate(end) - startHour + 1;
        int size = (int) hourDifference(start, end);
        
        this.list = new ArrayList<>(size);

        this.lightInterval = lightSlotInterval;

        // Fill with default values.
        for (long t = this.start.getTime(); t <= this.end.getTime(); t += lightInterval.toMS()) {
            list.add(new Sample<>(new Date(t), Switch.NA));
        }
    }

    public LightPlan(LightPlan copyFrom) {
        this.lightInterval = copyFrom.lightInterval;
        this.startHour = copyFrom.startHour;
        this.start = copyFrom.start;
        this.end = copyFrom.end;
        this.list = new ArrayList<>(copyFrom.list);
    }

    //
    // Calculations
    //
    public Duration getTotalLightOnDuration() {

        long totalMS = 0;
        int size = size();
        for (int i = 0; i < size; i++) {
            Switch s = getElement(i);
            if (s.isOn()) {
                totalMS += lightInterval.toMS();
            }
        }

        return new Duration(totalMS);
    }

    //
    // Accessors.
    //
    public Switch getStatusNow() {
        return size() > 0 ? getElement(0) : Switch.OFF;
    }

    public int size() {
        return list.size();
    }

    public List<Sample<Switch>> getList() {
        return Collections.unmodifiableList(list);
    }

    public Switch getElement(int idx) {
        Sample<Switch> r = list.get(idx);
        if (r != null) {
            return r.getSample();
        } else {
            return null;
        }
    }

    public Switch getElement(Date d) {
        Switch r = null;
        for (Sample<Switch> sample : list) {
            if (sample.getTimestamp().equals(d)) {
                r = sample.getSample();
            }
        }
        return r;
    }

    /**
     * @param idx Index of light plan
     * @return Start hour for the given light plan index.
     */
    public Date getElementStart(int idx) {
        return new Date(start.getTime() + idx * lightInterval.toMS());
    }

    public boolean hasDate(Date timeDate) {
        boolean r = false;
        for (Sample<Switch> sample : list) {
            try {
            if (sample.getTimestamp().equals(timeDate)) {
                r = true;
            }
            } catch(Exception ex) {
                System.out.println();
            }
        }
        return r;
    }

    public Duration getPlanDuration() {
        return new Duration(size() * lightInterval.toMS());
    }

    //
    // Modifiers.
    //
    public void setElement(int idx, Switch e) {
        this.list.set(idx, new Sample<>(getElementStart(idx), e));  //qqq getElementStart(idx): return the start time of index
    }

    public Duration getLightInterval() {
        return lightInterval;
    }

    //
    // Overrides
    //
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Sample<Switch> s : getList()) {
            Switch value = s.getSample();
            if (value.isNotAvailable()) {
                sb.append("?");
            } else {
                sb.append(value.isOn() ? "1" : "0");
            }
        }
        sb.append("]");
        sb.append(" start=").append(this.start);
        sb.append(" size = ").append(getList().size());
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.start);
        hash = 89 * hash + Objects.hashCode(this.list);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LightPlan other = (LightPlan) obj;
        if (!Objects.equals(this.start, other.start)) {
            return false;
        }
        if (!Objects.equals(this.list, other.list)) {
            return false;
        }
        return true;
    }

}
