package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class UMolSqrMeter implements Unit {

    private final BigDecimal value;

    public UMolSqrMeter(BigDecimal value) {
        this.value = value;
    }

    public UMolSqrMeter(Double value) {
        this(valueOf(value));
    }

    public UMolSqrMeter(double value) {
        this(valueOf(value));
    }

    @Override
    public String toString() {
        return String.format("%.1f [umol m^-2]", value());
    }

    public UMolSqrMeter add(UMolSqrMeter added) {
        return new UMolSqrMeter(this.value.add(added.value));
    }

    public MolSqrMeter toMol() {
        BigDecimal exp = BigDecimal.valueOf(Math.pow(10, -6));
        BigDecimal r = this.value.multiply(exp);
        return new MolSqrMeter(r);
    }

    public MMolSqrMeter toMMol() {
        BigDecimal exp = BigDecimal.valueOf(Math.pow(10, -3));
        BigDecimal r = this.value.multiply(exp);
        return new MMolSqrMeter(r);
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(2, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }
}
