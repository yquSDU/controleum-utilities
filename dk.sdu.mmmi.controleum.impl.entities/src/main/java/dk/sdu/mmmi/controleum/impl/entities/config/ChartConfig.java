package dk.sdu.mmmi.controleum.impl.entities.config;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The configuration of a chart, e.g. a name and IDs for a set of dataseries.
 *
 * @author mrj
 */
public class ChartConfig implements Serializable {

    private Integer id;
    private String name;
    private Set<String> series = new HashSet<>();
    private long period = 1000 * 60 * 60 * 24; // Default is 1 day
    private Date toDate = new Date();
    private int tabPosition;

    public ChartConfig() {
    }

    public ChartConfig(ChartConfig cfg) {
        this.id = cfg.id;
        this.toDate = cfg.toDate;
        this.name = cfg.name;
        this.series = new HashSet<>(cfg.series);
        this.period = cfg.period;
    }

    public void setID(int id) {
        this.id = id;
    }

    public Integer getID() {
        return id;
    }

    public String getIDString() {
        return String.valueOf(id);
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addSeries(String id) {
        series.add(id);
    }

    public void removeSeries(String id) {
        series.remove(id);
    }

    public void clearSeries() {
        series.clear();
    }

    public Set<String> getSeries() {
        return series;
    }

    public String getName() {
        return name;
    }

    public long getPeriod() {
        return period;
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    public Date getToDate() {
        return this.toDate;
    }

    public boolean hasName() {
        return name != null;
    }
//    @Override
//    public boolean equals(Object o) {
//        if (o instanceof ChartConfig) {
//            ChartConfig chart = (ChartConfig) o;
//            return Objects.equal(id, chart.id);
//        }
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(id);
//    }
}
