package dk.sdu.mmmi.controleum.impl.entities.results;

import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Celcius;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Outdoor temperature forecast.
 *
 * @author yqu
 */
public final class OutdoorTemperatureForecast {

    private List<Sample<Celcius>> forecast;

    public OutdoorTemperatureForecast() {
        forecast = new ArrayList<>();
    }

    public OutdoorTemperatureForecast(List<Sample<Celcius>> forecast) {
        this.forecast = forecast;
    }

    public List<Sample<Celcius>> get() {
        return forecast;
    }

    /**
     * @param from Start of forecast.
     * @param to End of forecast.
     * @return Forecast for the period [from;to].
     */
    public OutdoorTemperatureForecast getPeriod(Date from, Date to) {

        List<Sample<Celcius>> r = new ArrayList<>();

        for (Sample<Celcius> sample : forecast) {

            Date sampleTime = sample.getTimestamp();

            if ((sampleTime.equals(from) || sampleTime.after(from))
                    && (sampleTime.equals(to) || sampleTime.before(to))) {
                r.add(sample);
            }
        }
        return new OutdoorTemperatureForecast(r);
    }

    @Override
    public String toString() {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Sample<Celcius> t : forecast) {
            sb.append(df.format(t.getTimestamp()));
            sb.append("=");
            Celcius temperature = t.getSample();
            sb.append(temperature);
            sb.append(",    ");
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.forecast);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OutdoorTemperatureForecast other = (OutdoorTemperatureForecast) obj;
        if (!Objects.equals(this.forecast, other.forecast)) {
            return false;
        }
        return true;
    }
}
