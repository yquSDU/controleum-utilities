package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;

/**
 *
 * @author jcs
 */
public class MegaWattHour implements Unit {

    private final BigDecimal value;

    public MegaWattHour(double value) {
        this(valueOf(value));
    }

    public MegaWattHour(Double value) {
        this(valueOf(value));
    }

    public MegaWattHour(BigDecimal wattHour) {
        this.value = wattHour;
    }

    @Override
    public double value() {
        return value.doubleValue();
    }

    @Override
    public double roundedValue() {
        return value.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%.1f [MWh]", value());
    }
}
