package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Duration;
import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.math.RoundingMode;
import java.util.Objects;

/**
 *
 * @author jcs
 */
public class MoneyMegaWattHour implements Unit {

    private final Money value;

    /**
     *
     * @param value Value of currency
     * @param currencyCode The ISO 4217 code of the currency
     */
    public MoneyMegaWattHour(double value, String currencyCode) {
        this(valueOf(value), currencyCode);
    }

    public MoneyMegaWattHour(Double value, String currencyCode) {
        this(valueOf(value), currencyCode);
    }

    public MoneyMegaWattHour(BigDecimal value, String currencyCode) {
        this.value = new Money(value, currencyCode);
    }

    private MoneyMegaWattHour(Money value) {
        this.value = value;
    }

    public Money getValue() {
        return value;
    }

    public MoneyMegaWattHour plus(MoneyMegaWattHour price) {
        return new MoneyMegaWattHour(this.value.plus(price.value));
    }

    public MoneyMegaWattHour times(Duration duration) {
        return new MoneyMegaWattHour(value.times(duration.toHours()));
    }

    public Money times(MegaWattHour wattHour) {
        return this.value.times(wattHour.bigDecimal());
    }

    @Override
    public String toString() {
        return String.format("%1$.1f [%2$s/MWh]", value.value(), value.getCurrency());
    }

    @Override
    public double value() {
        return value.value();
    }

    @Override
    public double roundedValue() {
        return bigDecimal().setScale(2, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return value.bigDecimal();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MoneyMegaWattHour other = (MoneyMegaWattHour) obj;
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
}
