package dk.sdu.mmmi.controleum.impl.entities.units;

import dk.sdu.mmmi.controleum.common.units.Unit;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author mrj
 */
public class PPM implements Unit {

    private final BigDecimal ppm;

    public PPM(BigDecimal ppm) {
        this.ppm = ppm;
    }

    public PPM(Double ppm) {
        this(BigDecimal.valueOf(ppm));
    }

    public PPM(double ppm) {
        this(BigDecimal.valueOf(ppm));
    }

    @Override
    public String toString() {
        return String.format("%.1f [PPM]", value());
    }

    @Override
    public double value() {
        return ppm.doubleValue();
    }

    @Override
    public double roundedValue() {
        return ppm.setScale(1, RoundingMode.UP).doubleValue();
    }

    @Override
    public BigDecimal bigDecimal() {
        return ppm;
    }

    public PPM subtract(PPM s) {
        return new PPM(ppm.subtract(s.bigDecimal()));
    }

}
