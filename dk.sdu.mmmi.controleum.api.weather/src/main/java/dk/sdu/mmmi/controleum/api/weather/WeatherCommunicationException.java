package dk.sdu.mmmi.controleum.api.weather;

/**
 *
 * @author martinmm
 */
public class WeatherCommunicationException extends Exception {

    public WeatherCommunicationException() {
        super();
    }
    public WeatherCommunicationException(Throwable cause)
    {
        super(cause);
    }
    public WeatherCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }
    public WeatherCommunicationException(String message)
    {
        super(message);
    }
}
