/*
 * DataSource.java
 *
 * Created on 15. august 2006, 09:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.weather;

import java.io.Serializable;

/**
 *
 * @author skalita
 */
public class DataSource implements Comparable, Serializable {

    private Long id;
    private String label;
    private String hostname;
    private Integer port;
    private String dbname;
    private String username;
    private String password;
    private String backingStoreName;
    private String dbtype;
    private byte[] encryptedPassword;
    public static final String WEATHER_DB = "weatherDB";
    public static final String ELECTRICITY_PRICE_DB = "electricityPriceDB";

    /**
     * Creates a new instance of Compartment
     */
    public DataSource(String label) {
        this.setLabel(label);
        this.setHostname("");
        //this.setPort(-1);
        this.setDbname("");
        this.setUsername("");
        this.setPassword("");
        //this.setId(-1);
    }

    public DataSource() {
        new DataSource("");
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDbname() {
        return dbname;
    }

    public String getHostname() {
        return hostname;
    }

    public String getLabel() {
        return label;
    }

    public String getPassword() {
        return password;
    }

    public Integer getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String toString() {
        return getLabel();
    }

    public String toHtmlString() {
        return "<b>" + getLabel() + "</b>";
    }

    public int compareTo(Object o) {
        DataSource c = (DataSource) o;
        return this.getLabel().compareTo(c.getLabel());
    }

    public String getBackingStoreName() {
        return backingStoreName;
    }

    public void setBackingStoreName(String backingStoreName) {
        this.backingStoreName = backingStoreName;
    }

    public String getDbtype() {
        return dbtype;
    }

    public void setDbtype(String dbtype) {
        this.dbtype = dbtype;
    }

    public byte[] getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(byte[] encryptedPassword) {
        //if(encryptedPassword.length == 0) return;
        this.encryptedPassword = encryptedPassword;
        password = new String(Crypto.decrypt(encryptedPassword));
    }
}
