/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.api.weather;

import dk.sdu.mmmi.controleum.api.weather.DataSource;



/**
 *
 * @author martinmm
 */
public class DataSourceSetting extends DataSource{

    private String specialPrefix;

    /**
     * Special prefix can be used in cases where data is stored
     * in tables that uses e.g. the growers name as part of the table
     * names. An example is the weatherdata base where every grower
     * has its own table and the distinction between them is based on
     * a db table prefix, e.g. alfpedersen_tablename.
     * @return the specialPrefix
     */
    public String getSpecialPrefix() {
        return specialPrefix;
    }

    /**
     * Special prefix can be used in cases where data is stored
     * in tables that uses e.g. the growers name as part of the table
     * names. An example is the weatherdata base where every grower
     * has its own table and the distinction between them is based on
     * a db table prefix, e.g. alfpedersen_tablename.
     * @param specialPrefix the specialPrefix to set
     */
    public void setSpecialPrefix(String specialPrefix) {
        this.specialPrefix = specialPrefix;
    }



}
