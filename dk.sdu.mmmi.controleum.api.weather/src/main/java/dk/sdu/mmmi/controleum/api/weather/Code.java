/*
 * Code.java
 *
 * Created on 15. august 2006, 09:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.api.weather;

/**
 *
 * @author skalita
 */
public class Code implements Comparable<Code>{

    private int id;
    private String name;
    private String unit;

    /** Creates a new instance of Code */
    public Code(int id, String name, String unit) {
        this.id = id;
        this.name = name;
        this.unit = unit;
    }

    public Code(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Code){
            Code c = (Code)obj;
            return this.id == c.getId();
        }
        return false;
    }

    public String toString(){
        return "(" + id + ")  " + name ;
    }

    public String toHtmlString() {
        return "(" + id + ")  " + name + "  [" + unit + "]"  ;
    }

    /*public int compareTo(Object o) {
        Code code = (Code)o;
        return this.getId() - code.getId();
    }*/

    public int compareTo(Code o) {
        return this.getId() - o.getId();
    }

}
