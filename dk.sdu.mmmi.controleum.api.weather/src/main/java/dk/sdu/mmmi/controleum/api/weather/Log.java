/*
 * Log.java
 *
 * Created on 15. august 2006, 09:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.api.weather;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author skalita
 */
public class Log {

    private long time;
    private double value;

    /** Creates a new instance of Log */
    public Log(long time, double value) {
        this.time=time;
        this.value=value;
    }

    public long getTime() {
        return time;
    }

    public String getTimeString(){
        return new SimpleDateFormat("dd-MM-yy HH:mm").format(new Date(time));
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getValueString() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(4);
        return nf.format(value);
    }

    public String toString(){
        return new Date(time).toString() + " : " + value;
    }
}
