/*
 * Crypto.java
 *
 * Created on 26. april 2007, 09:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.api.weather;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author skalita
 */
public class Crypto {

   private static final String CRYPTO_ALGO= "AES";
    private static byte[] keyBytes = {6,0,5,9,3,5,6,1,8,4,0,4,7,2,1,6};

    public static byte[] encrypt(byte[] arg)
    {
    	SecretKey secretKey = new SecretKeySpec(keyBytes, CRYPTO_ALGO);
        Cipher cipher;
        byte[] encrypted = null;

        try {
            cipher = Cipher.getInstance(CRYPTO_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encrypted = cipher.doFinal(arg);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        return encrypted;
    }

    public static byte[] decrypt(byte[] arg){

        SecretKey secretKey = new SecretKeySpec(keyBytes, CRYPTO_ALGO);
        Cipher cipher;
        byte[] decrypted = null;

        try {
            cipher = Cipher.getInstance(CRYPTO_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            SecretKey regeneratedSecretKey = new SecretKeySpec(keyBytes, CRYPTO_ALGO);
            cipher.init(Cipher.DECRYPT_MODE, regeneratedSecretKey);
            decrypted = cipher.doFinal(arg);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        return decrypted;
    }

}
