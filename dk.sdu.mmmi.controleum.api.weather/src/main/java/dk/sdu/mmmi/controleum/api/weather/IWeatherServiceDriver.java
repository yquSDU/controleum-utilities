package dk.sdu.mmmi.controleum.api.weather;

import dk.sdu.mmmi.controleum.common.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.config.LightForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.config.TemperatureForecastConfig;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorLightForecast;
import dk.sdu.mmmi.controleum.impl.entities.results.OutdoorTemperatureForecast;
import java.util.Date;

/**
 *
 * @author sant1979, hmmm, jcs
 */
public interface IWeatherServiceDriver {

    /**
     *
     * @param cfg Configuration of weather service.
     * @param from Start timestamp of weather data
     * @param to End timestamp of weather.
     * @return Weather data in UTC time.
     * @throws WeatherCommunicationException
     */
    Sample<OutdoorLightForecast> getOutdoorLightForecast(LightForecastConfig cfg, Date from, Date to) throws WeatherCommunicationException;
    Sample<OutdoorTemperatureForecast> getOutdoorTemperatureForecast(TemperatureForecastConfig cfg, Date from, Date to);
    
}
