/*
 * WeatherServiceProvider.java
 *
 * Created on 30. maj 2008, 12:36
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.weather;

import java.util.Collection;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Lookup;

/**
 *
 * @author sant1979
 * getDefault re-implemented by hmmm 28-03-2011
 */
public class WeatherServiceProvider {

    /**
     * Creates a new instance of WeatherServiceProvider
     */
    public WeatherServiceProvider() {
    }

    public static IWeatherServiceDriver getDefault() {

        Collection<? extends IWeatherServiceDriver> lookupAll = Lookup.getDefault().lookupAll(IWeatherServiceDriver.class);
        if (lookupAll.size() > 1 || lookupAll.isEmpty()) {
            String msg = "There are none or multiple weather database service provider(s). E.g. LightPlanMaker can only handle a single.";
            NotifyDescriptor nd = new NotifyDescriptor.Message(msg);
            DialogDisplayer.getDefault().notify(nd);
        } else {
            return lookupAll.iterator().next();
        }
        return null;
    }
}
