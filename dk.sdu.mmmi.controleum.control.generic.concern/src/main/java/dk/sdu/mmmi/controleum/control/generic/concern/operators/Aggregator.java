/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern.operators;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import dk.sdu.mmmi.controleum.control.generic.ControleumValueListImpl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * TODO: Change return type. Always just a list - never a list of lists (except
 * when doing nothing - but it is never called
 *
 * @author ancla
 */
public enum Aggregator {

    NONE {

                @Override
                public List<ControleumValueList> aggregate(List<ControleumValueList> values) {
                    throw new UnsupportedOperationException("The None-Aggregator should not be called.");
                }

                @Override
                public String toString() {
                    return "None";
                }

            },
    SUM {
                List<ControleumValueList> v = new ArrayList<>();
                ControleumValueList baseList = new ControleumValueListImpl();

                @Override
                public List<ControleumValueList> aggregate(List<ControleumValueList> values) {
                    /*  baseList.clear();
                     ControleumValue[] result = new ControleumValue[values.get(0).size()];
                     Arrays.fill(result, new ControleumValue(0));
                     values.stream().forEach((l) -> {
                     //DET ER BARE DUMT.
                     Arrays.setAll(result, i -> result[i].addControleumValues(l.get(i)));
                     });
                     baseList.addAll(result);
                     */
                    //return values.stream().reduce(new ControleumValueListImpl(), (a, b) -> a.addedWith(b));
                    //return values.stream().collect(.new ControleumValueListImpl(), (a, b) -> a.addedWith(b));
                    return Arrays.asList(values.stream().reduce(new ControleumValueListImpl(), (a, b) -> a.addedWith(b)));
                    //return baseList;
                }

                @Override
                public String toString() {
                    return "Sum of columns";
                }
            }/*,
     LEFT_TO_LEFT_DIAGONAL {
     ControleumValueListImpl baseList = new ControleumValueListImpl();

     @Override
     public ControleumValueListImpl aggregate(List<ControleumValueList> values) {
     baseList.clear();
     int height = values.size();
     int width = values.get(0).size();
     if (height != width) {
     return baseList;
     } else {
     for (int col = 0; col < height; col++) {
     int row = col;
     int res = values.get(row).get(col).getValue().intValue();
     for (int j = 0; j < col; j++) {
     int num = j + 1;
     if (col + num > (height - 1)) {
     break;
     }
     res += values.get(row + num).get(col - num).getValue().intValue();
     res += values.get(row - num).get(col + num).getValue().intValue();

     }
     baseList.add(new ControleumValue(res));
     }

     }
     return baseList;
     }

     @Override
     public String toString() {
     return "Left to Right-diagonal";
     }
     },
     RIGHT_TO_RIGHT_DIAGONAL {
     ControleumValueListImpl baseList = new ControleumValueListImpl();

     @Override
     public ControleumValueListImpl aggregate(List<ControleumValueList> values) {
     baseList.clear();
     int height = values.size();
     int width = values.get(0).size();
     if (height != width) {
     return baseList;
     } else {
     for (int col = 0; col < height; col++) {
     int row = (height - 1) - col;
     int res = values.get(row).get(col).getValue().intValue();
     for (int j = 0; j < col; j++) {
     int num = j + 1;
     if (col + num > height - 1) {
     break;
     }
     res += values.get(row - num).get(col - num).getValue().intValue();
     res += values.get(row + num).get(col + num).getValue().intValue();
     }
     baseList.add(new ControleumValue(res));
     }
     }
     return baseList;
     }

     @Override
     public String toString() {
     return "Right to Left-diagonal";
     }
     },
     CHESSBOARD_NORTH_EAST {
     ControleumValueListImpl baseList = new ControleumValueListImpl();

     @Override
     public ControleumValueListImpl aggregate(List<ControleumValueList> values) {
     baseList.clear();
     int maxRowIndex = values.size() - 1;
     int maxColIndex = values.get(0).size() - 1;
     if (maxRowIndex != maxColIndex) {
     return baseList;
     } else {
     for (int row = 0; row <= maxRowIndex; row++) {
     int res = values.get(row).get(0).getValue().intValue();
     for (int i = 0; i < row; i++) {
     int num = i + 1;
     res += values.get(row - num).get(num).getValue().intValue();
     }
     baseList.add(new ControleumValue(res));
     int col = maxColIndex - row;
     if (col != 0) {
     int resb = values.get(maxRowIndex).get(col).getValue().intValue();
     for (int i = 0; i < row; i++) {
     int num = i + 1;
     resb += values.get(maxRowIndex - num).get(col + num).getValue().intValue();
     }
     baseList.add(new ControleumValue(resb));
     }
     }
     }
     return baseList;
     }

     @Override
     public String toString() {
     return "Chessboard North East Sum";
     }
     },
     CHESSBOARD_SOUTH_EAST {
     ControleumValueListImpl baseList = new ControleumValueListImpl();

     @Override
     public ControleumValueListImpl aggregate(List<ControleumValueList> values) {
     baseList.clear();
     int maxRowIndex = values.size() - 1;
     int maxColIndex = values.get(0).size() - 1;
     if (maxRowIndex != maxColIndex) {
     return baseList;
     } else {
     for (int row = 0; row <= maxRowIndex; row++) {
     int idx = maxRowIndex - row;
     int res = values.get(idx).get(0).getValue().intValue();
     for (int i = 0; i < row; i++) {
     int num = i + 1;
     res += values.get(idx + num).get(num).getValue().intValue();
     }
     baseList.add(new ControleumValue(res));
     int col = maxColIndex - row;
     if (col != 0) {
     int resb = values.get(0).get(col).getValue().intValue();
     for (int i = 0; i < row; i++) {
     int num = i + 1;
     resb += values.get(num).get(col + num).getValue().intValue();
     }
     baseList.add(new ControleumValue(resb));
     }
     }
     }
     return baseList;
     }

     @Override
     public String toString() {
     return "Chessboard South East Sum";
     }
     }*/;

    public abstract List<ControleumValueList> aggregate(List<ControleumValueList> values);

    @Override
    public abstract String toString();
}
