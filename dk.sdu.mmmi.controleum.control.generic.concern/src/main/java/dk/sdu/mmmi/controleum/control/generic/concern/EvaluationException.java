/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;

/**
 *
 * @author Anders
 */
public class EvaluationException extends Exception {
    public EvaluationException(String message)
    {
        super(message);
    }
}
