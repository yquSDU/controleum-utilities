/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;

import dk.sdu.mmmi.controleum.api.control.generic.BasicListener;

/**
 *
 * @author ancla
 */
public interface ConcernConfigEventListener extends BasicListener<ConcernConfigEvent> {

}
