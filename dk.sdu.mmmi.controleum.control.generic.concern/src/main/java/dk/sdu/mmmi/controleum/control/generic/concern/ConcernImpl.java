/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;


import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.control.generic.concern.operators.*;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.sun.media.jfxmedia.logging.Logger;
import dk.sdu.mmmi.controleum.control.generic.hazelcast.ControleumValueInput;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssue;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.ancla.AgentSetup;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.util.ArrayList;
import java.util.List;

import java.util.UUID;
import org.openide.util.Exceptions;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.moea.Value;
import dk.sdu.mmmi.controleum.control.generic.ControleumValueListImpl;

/**
 *
 * @author ancla
 */
public class ConcernImpl extends AbstractConcernAgent implements AgentSetup {

    static HazelcastInstance client = HazelcastClient.newHazelcastClient(new ClientConfig());

    private final List<ControleumValueList> suggestedValueVectors;
    private List<ControleumValueInput> targetVectorInputs;
    private List<ControleumValueInput> coefficientVectorInputs;

    private List<ControleumValueList> targetVectors = new ArrayList<>();
    private List<ControleumValueList> coefficientVectors = new ArrayList<>();

    private ConcernConfig cfg;
    private final IMap<UUID, ControleumValueList> distMap;
    private Distance distance;
    private Bound bound;
    private EnvironmentModel environmentModel;
    private Aggregator a;
    private Contribution c;
    private final ControleumContext cd;

    public ConcernImpl(ControleumContext cd, ConcernConfig cfg) {
        super(cfg.getName(), cd, cfg.getId());
        this.cd = cd;
        setConfig(cfg);
        distMap = client.getMap(cfg.getMapName());
        suggestedValueVectors = new ArrayList<>();
    }

    public final void setConfig(ConcernConfig cfg) {
        this.cfg = cfg;
        setName(cfg.getName());
        setEnabled(cfg.isEnabled());
        setPriority(cfg.getPriority());
        this.distance = cfg.getDistanceOperator();
        this.bound = cfg.getBoundaryOperator();
        this.a = cfg.getAggregateOperator();
        this.c = cfg.getContributionOperator();
        //this.environmentModel = cfg.getModelOperator();
    }

    @Override
    public double evaluate(Solution option) throws IllegalStateException {
        ISolution is = (ISolution) option;
        if (targetVectorInputs != null && targetVectors.size() > 0) {
            loadSuggestedValues(is, cfg.getIssueIDs());
            try {
                if (environmentModel != null && cfg.getModelOperator() != Model.NONE) {
                    try {

                        List<ControleumValueList> valStream = environmentModel.calculate(suggestedValueVectors);
                        if (suggestedValueVectors.size() > 1) {
                            Logger.logMsg(LOW_PRIORITY, "test");
                        }
                        return CostFunction.calculateDistance(valStream, targetVectors, bound, distance, a, c);
                    } catch (Exception ex) {
                        Exceptions.printStackTrace(ex);
                    }
                } else {
                    return CostFunction.calculateDistance(suggestedValueVectors, targetVectors, bound, distance, a, c);
                }
            } catch (EvaluationException ex) {
                System.out.println("Unable to perform evaluation. Staying out of negotiation.");
                Exceptions.printStackTrace(ex);
                return Double.MAX_VALUE;
            }
        } else {
            //No preferences received yet. Return max fitness, to replace solutions when preferences arrives.
            return Double.MAX_VALUE;
        }

        //Never reached
        return Double.MAX_VALUE;
    }

    private void loadSuggestedValues(ISolution option, List<UUID> issueIDs) {
        suggestedValueVectors.clear();
        for (GenericIssue issue : context(cd).all(GenericIssue.class)) {
            if (issueIDs.contains(issue.getIssueId())) {
                suggestedValueVectors.add(new ControleumValueListImpl((option.getValue((Value<ControleumValueList>)issue)).getValues()));
            }
        }
    }

    @Override
    public void doUpdate() {
        targetVectors.clear();
        coefficientVectors.clear();
        for (ControleumValueInput input : targetVectorInputs) {
            if (input.getValue() != null) {
                targetVectors.add((ControleumValueList) input.getValue());
            }
        }

        for (ControleumValueInput input : coefficientVectorInputs) {
            if (input.getValue() != null) {
                coefficientVectors.add((ControleumValueList) input.getValue());
            }
        }
        updateEnvironmentModel();
    }

    @Override
    public void doSetup() {
        //TODO Make async (subscribe to inputs instead)
        targetVectorInputs = new ArrayList<>();
        coefficientVectorInputs = new ArrayList<>();
        for (ControleumValueInput input : context(g).all(ControleumValueInput.class)) {
            if (cfg.getTargetVectorInputIDs().contains(input.getId())) {
                targetVectorInputs.add(input);
            } else if (cfg.getCoefficientsId().contains(input.getId())) {
                coefficientVectorInputs.add(input);
            }
        }
        doUpdate();
        updateEnvironmentModel();
    }

    private void updateEnvironmentModel() {
        //TODO Lookup instead. ?
        if (cfg.getModelOperator() == Model.MULTIPLY) {
            this.environmentModel = new MultiplicationModel(UUID.randomUUID(), coefficientVectors);
        }
    }

    @Override
    public boolean isSubsystemAgent() {
        return cfg.isSubsystemAgent();
    }

}
