/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern.operators;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;

/**
 * Implementation of some of the distance operators explained here
 * http://numerics.mathdotnet.com/Distance.html
 *
 * @author ancla
 */
public enum Distance {

    //Returns sum of absolute difference between @preferences and @suggestedValue.
    MANHATTAN {
                @Override
                public double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound) {
                    double val = 0;
                    double suggestedValue;
                    double preferredValue;
                    for (int i = 0; i < preferences.size(); i++) {
                        try {
                            suggestedValue = suggestedValues.get(i).doubleValue();
                            preferredValue = preferences.get(i).doubleValue();
                            if (bound.outOfBounds(suggestedValue, preferredValue)) {
                                val += Math.abs(suggestedValue - preferredValue);;
                            }
                        } catch (Exception ex) {
                            System.out.println(ex);
                        }
                    }
                    return val;
                }

                @Override
                public String toString() {
                    return "Manhattan";
                }
            },
    NORMALIZED_MANHATTAN {
                @Override
                public double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound) {
                    double val = 0;
                    double suggestedValue;
                    double preferredValue;
                    double n = preferences.size();
                    for (int i = 0; i < n; i++) {
                        suggestedValue = suggestedValues.get(i).doubleValue();
                        preferredValue = preferences.get(i).doubleValue();
                        if (bound.outOfBounds(suggestedValue, preferredValue)) {
                            val += Math.abs(suggestedValue - preferredValue);;
                        }
                    }
                    return val / n;
                }

                @Override
                public String toString() {
                    return "Normalized Manhattan";
                }
            },
    EUCLIDEAN {
                @Override
                public double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound) {
                    double val = 0;
                    double suggestedValue;
                    double preferredValue;
                    for (int i = 0; i < preferences.size(); i++) {
                        suggestedValue = suggestedValues.get(i).doubleValue();
                        preferredValue = preferences.get(i).doubleValue();
                        if (bound.outOfBounds(suggestedValue, preferredValue)) {
                            val += Math.pow(suggestedValue - preferredValue, 2);
                        }
                    }
                    return Math.sqrt(val);
                }

                @Override
                public String toString() {
                    return "Euclidean";
                }

            },
    NORMALIZED_EUCLIDEAN {
                @Override
                public double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound) {
                    double val = 0;
                    double suggestedValue;
                    double preferredValue;
                    for (int i = 0; i < preferences.size(); i++) {
                        suggestedValue = suggestedValues.get(i).doubleValue();
                        preferredValue = preferences.get(i).doubleValue();
                        if (bound.outOfBounds(suggestedValue, preferredValue)) {
                            val += Math.pow(suggestedValue - preferredValue, 2);
                        }
                    }
                    return Math.sqrt(val / preferences.size());
                }

                @Override
                public String toString() {
                    return "Normalized Euclidean";
                }

            },
    SQUARED_EUCLIDEAN {
                @Override
                public double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound) {
                    double val = 0;
                    double suggestedValue;
                    double preferredValue;
                    for (int i = 0; i < preferences.size(); i++) {
                        suggestedValue = suggestedValues.get(i).doubleValue();
                        preferredValue = preferences.get(i).doubleValue();
                        if (bound.outOfBounds(suggestedValue, preferredValue)) {
                            val += Math.pow(suggestedValue - preferredValue, 2);
                        }
                    }
                    return val;
                }

                @Override
                public String toString() {
                    return "Squared Euclidean";
                }

            };
    /*CHEBYSHEV {
     @Override
     public double diff(ControleumValueListImpl preferences, ControleumValueListImpl suggestedValues, Bound bound) {
     double val = 0;
     double suggestedValue;
     double preferredValue;
     for (int i = 0; i < preferences.size(); i++) {
     suggestedValue = suggestedValues.get(i).getValue().doubleValue();
     preferredValue = preferences.get(i).getValue().doubleValue();
     if (bound.outOfBounds(suggestedValue, preferredValue)) {
     double amountShifted = Math.abs(suggestedValue - preferredValue);
     if (amountShifted > val) {
     val = amountShifted;
     }
     }
     }
     return val;
     }

     @Override
     public String toString() {
     return "Chebyshev";
     }

     };
     *//*CHEBYSHEV {
     @Override
     public double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound) {
     double val = 0;
     double suggestedValue;
     double preferredValue;
     for (int i = 0; i < preferences.size(); i++) {
     suggestedValue = suggestedValues.get(i).getValue().doubleValue();
     preferredValue = preferences.get(i).getValue().doubleValue();
     if (bound.outOfBounds(suggestedValue, preferredValue)) {
     double amountShifted = Math.abs(suggestedValue - preferredValue);
     if (amountShifted > val) {
     val = amountShifted;
     }
     }
     }
     return val;
     }

     @Override
     public String toString() {
     return "Chebyshev";
     }

     };
     */

    public abstract double diff(ControleumValueList preferences, ControleumValueList suggestedValues, Bound bound);

}
