/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;


import dk.sdu.mmmi.controleum.api.control.generic.BasicConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import dk.sdu.mmmi.controleum.control.generic.concern.operators.*;

/**
 *
 * @author ancla
 */
public class ConcernConfig extends BasicConfig {

    private List<UUID> preferenceIDs;
    private List<UUID> issueIDs;
    private List<UUID> coefficientsId;
    private int priority;
    private boolean enabled;
    private boolean isSubsystemAgent;

    private Aggregator aggregate;
    private Distance distance;
    private Bound boundary;
    private Model model;
    private Contribution contributor;

    public ConcernConfig() {
        super("Unnamed");
        this.preferenceIDs = new ArrayList<>();
        this.issueIDs = new ArrayList<>();
        this.coefficientsId = new ArrayList<>();
        this.priority = 1;
        this.enabled = false;
        this.distance = Distance.MANHATTAN;
        this.boundary = Bound.EQUAL;
        this.aggregate = Aggregator.NONE;
        this.model = Model.NONE;
        this.contributor = Contribution.NONE;
        this.isSubsystemAgent = false;
    }

    public ConcernConfig(String name) {
        super(name);
        this.preferenceIDs = new ArrayList<>();
        this.issueIDs = new ArrayList<>();
        this.coefficientsId = new ArrayList<>();
        this.priority = 1;
        this.enabled = false;
        this.distance = Distance.MANHATTAN;
        this.boundary = Bound.EQUAL;
        this.aggregate = Aggregator.NONE;
        this.contributor = Contribution.NONE;
        this.model = Model.NONE;
        this.isSubsystemAgent = false;
    }

    public ConcernConfig(String name, List<UUID> preferenceIDs, List<UUID> issueIDs, int priority, boolean enabled) {
        super(name);
        this.preferenceIDs = preferenceIDs;
        this.issueIDs = issueIDs;
        this.priority = priority;
        this.enabled = enabled;
        this.distance = Distance.MANHATTAN;
        this.boundary = Bound.EQUAL;
        this.aggregate = Aggregator.NONE;
        this.contributor = Contribution.NONE;
        this.model = Model.NONE;
        this.isSubsystemAgent = false;
    }

    public ConcernConfig(UUID id, String name, List<UUID> preferenceIDs, List<UUID> issueIDs, int priority, boolean enabled) {
        super(id, "Unnamed");
        this.preferenceIDs = preferenceIDs;
        this.issueIDs = issueIDs;
        this.priority = priority;
        this.enabled = enabled;
        this.distance = Distance.MANHATTAN;
        this.boundary = Bound.EQUAL;
        this.aggregate = Aggregator.NONE;
        this.contributor = Contribution.NONE;
        this.model = Model.NONE;
        this.isSubsystemAgent = false;
    }

    public ConcernConfig(UUID id, String name, List<UUID> preferenceIDs, List<UUID> issueIDs, int priority, boolean enabled, Distance distance, Aggregator aggregator, Bound boundary, Model model, List<UUID> coefficientsID) {
        super(id, name);
        this.preferenceIDs = preferenceIDs;
        this.issueIDs = issueIDs;
        this.priority = priority;
        this.enabled = enabled;
        this.distance = distance;
        this.aggregate = aggregator;
        this.contributor = Contribution.NONE;
        this.boundary = boundary;
        this.model = model;
        this.coefficientsId = coefficientsID;
        this.isSubsystemAgent = false;
    }

    public ConcernConfig(UUID id, String name, List<UUID> preferenceIDs, List<UUID> issueIDs, int priority, boolean enabled, Distance distance, Aggregator aggregator, Bound boundary, Model model, List<UUID> coefficientsID, boolean isSubsystemAgent) {
        super(id, name);
        this.preferenceIDs = preferenceIDs;
        this.issueIDs = issueIDs;
        this.priority = priority;
        this.enabled = enabled;
        this.distance = distance;
        this.aggregate = aggregator;
        this.boundary = boundary;
        this.model = model;
        this.contributor = Contribution.NONE;
        this.coefficientsId = coefficientsID;
        this.isSubsystemAgent = isSubsystemAgent;
    }

    /**
     * @return the preferenceIDs
     */
    public List<UUID> getTargetVectorInputIDs() {
        return preferenceIDs;
    }

    /**
     * @param preferenceIDs the preferenceIDs to set
     */
    public void setPreferenceIDs(List<UUID> preferenceIDs) {
        this.preferenceIDs = preferenceIDs;
    }

    /**
     * @return the issueIDs
     */
    public List<UUID> getIssueIDs() {
        return issueIDs;
    }

    /**
     * @param issueIDs the issueIDs to set
     */
    public void setIssueIDs(List<UUID> issueIDs) {
        this.issueIDs = issueIDs;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the operator
     */
    public Distance getDistanceOperator() {
        return distance;
    }

    /**
     * @param operator the operator to set
     */
    public void setDistanceOperator(Distance operator) {
        this.distance = operator;
    }

    public void setAggregateOperator(Aggregator aggregate) {
        this.aggregate = aggregate;
    }

    public Aggregator getAggregateOperator() {
        return this.aggregate;
    }

    public void setBoundaryOperator(Bound boundary) {
        this.boundary = boundary;
    }

    public Bound getBoundaryOperator() {
        return this.boundary;
    }

    public void setModelOperator(Model model) {
        this.model = model;
    }

    public Model getModelOperator() {
        return this.model;
    }

    public void setCoefficientsId(List<UUID> id) {
        this.coefficientsId = id;
    }

    public List<UUID> getCoefficientsId() {
        return this.coefficientsId;
    }

    public void setSubsystemAgent(boolean isSubsystemAgent) {
        this.isSubsystemAgent = isSubsystemAgent;
    }

    public boolean isSubsystemAgent() {
        return this.isSubsystemAgent;
    }

    public void setContributionOperator(Contribution contribution) {
        this.contributor = contribution;
    }
    
    public Contribution getContributionOperator()
    {
        return this.contributor;
    }
}
