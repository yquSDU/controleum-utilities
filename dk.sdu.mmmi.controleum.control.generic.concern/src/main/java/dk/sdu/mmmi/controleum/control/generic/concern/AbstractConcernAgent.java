/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.control.generic.concern.ConcernAgent;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 *
 * @author ancla
 */
public abstract class AbstractConcernAgent extends AbstractConcern implements ConcernAgent {

    private UUID issueId;
    private final UUID concernId;

    public AbstractConcernAgent(String name, ControleumContext g, UUID concernId) {
        super(name, g, 1);
        this.concernId = concernId;
    }


    public Map<UUID, String> getValueHelpUUID(Solution s) {
        return Collections.EMPTY_MAP;
    }

    /**
     * @return the concernId
     */
    @Override
    public UUID getConcernId() {
        return concernId;
    }

 }
