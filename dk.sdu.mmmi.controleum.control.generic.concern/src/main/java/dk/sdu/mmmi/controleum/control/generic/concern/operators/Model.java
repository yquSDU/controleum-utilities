/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern.operators;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author ancla
 */
public enum Model {

    NONE {
                @Override
                public List<ControleumValueList> f(List<ControleumValueList> values, List<ControleumValueList> coefficients) {
                    return values;
                }

                @Override
                public String toString() {
                    return "None";
                }

            },
    MULTIPLY {
                @Override
                public List<ControleumValueList> f(List<ControleumValueList> values, List<ControleumValueList> coefficients) {
                    if (coefficients.size() == 1) {
                        return values.stream()
                        .map((val) -> val.multipliedWith(coefficients.get(0)))
                        .collect(Collectors.toList());
                    } else {
                        return IntStream.range(0, values.size())
                        .mapToObj(i -> {
                            ControleumValueList n1 = values.get(i);
                            ControleumValueList n2 = coefficients.get(i);
                            return n2 == null ? n1 : n1.multipliedWith(n2);
                        }
                        )
                        .collect(Collectors.toList());
                    }

                }

                @Override
                public String toString() {
                    return "Multiply";
                }
            };

    public abstract List<ControleumValueList> f(List<ControleumValueList> values, List<ControleumValueList> coefficients) throws Exception;

    @Override
    public abstract String toString();
}
