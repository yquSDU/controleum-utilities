/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;


import dk.sdu.mmmi.controleum.control.generic.concern.operators.Aggregator;
import dk.sdu.mmmi.controleum.control.generic.concern.operators.Bound;
import dk.sdu.mmmi.controleum.control.generic.concern.operators.Distance;
import dk.sdu.mmmi.controleum.control.generic.concern.operators.Contribution;
import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.List;

/**
 *
 * @author Anders
 */
public class CostFunction {

    private static double calculateDistance(List<ControleumValueList> suggestedValueVectors, List<ControleumValueList> preferenceVector, Bound b, Distance d) throws EvaluationException {
        double val = 0.0;

        for (ControleumValueList suggestedValueVector : suggestedValueVectors) {
            if (preferenceVector.size() == 1) {
                val += d.diff(preferenceVector.get(0), suggestedValueVector, b);
            } else {
                if (preferenceVector.size() != suggestedValueVector.size()) {
                    throw new EvaluationException("Size of @preferenceVectors must be either 1 or same as @suggestedValuesVectors.size() (Number of issues subscribed)");
                }
                for (int i = 0; i < suggestedValueVectors.size(); i++) {
                    val += d.diff(preferenceVector.get(i), suggestedValueVectors.get(i), b);
                }
            }
        }

        //Assume same size of @preferenceVectors and @suggestedValueVectors.
        return val;
    }

    public static double calculateDistance(List<ControleumValueList> suggestedValueVectors, List<ControleumValueList> preferenceVector, Bound b, Distance d, Aggregator a) throws EvaluationException {
        if (a == Aggregator.NONE) {
            return calculateDistance(suggestedValueVectors, preferenceVector, b, d);
        } else {
            return calculateDistance(a.aggregate(suggestedValueVectors), a.aggregate(preferenceVector), b, d);
        }
    }

    public static double calculateDistance(List<ControleumValueList> suggestedValueVectors, List<ControleumValueList> preferenceVector, Bound b, Distance d, Aggregator a, Contribution c) throws EvaluationException {
        if (c == Contribution.NONE) {
            return calculateDistance(suggestedValueVectors, preferenceVector, b, d, a);
        } else {
            return calculateDistance(c.contribution(suggestedValueVectors), c.contribution(preferenceVector), b, d, a);
        }
    }
}
