/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern.operators;

import dk.sdu.mmmi.controleum.control.generic.ControleumValueListImpl;
import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Anders
 */
public enum Contribution {

    NONE {
                @Override
                public List<ControleumValueList> contribution(List<ControleumValueList> values) {
                    throw new UnsupportedOperationException("The None-Contributor should not be called.");
                }

                @Override
                public String toString() {
                    return "None";
                }

            },
    SUM_OF_ELEMENTS {
                ControleumValueListImpl baseList = new ControleumValueListImpl();

                @Override
                public List<ControleumValueList> contribution(List<ControleumValueList> values) {
                    return values.stream().map(l -> l.summarizedValue()).collect(Collectors.toCollection(ArrayList::new));
                }

                @Override
                public String toString() {
                    return "Sum of rows";
                }
            };

    public abstract List<ControleumValueList> contribution(List<ControleumValueList> values);

    @Override
    public abstract String toString();

}
