/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern.operators;

/**
 *
 * @author ancla
 */
public enum Bound {

        LESSTHAN {
                @Override
                public boolean outOfBounds(double suggestedValue, double preferredValue) {
                    return suggestedValue >= preferredValue;
                }

                @Override
                public String toString() {
                    return "Less Than Preference";
                }
            },
    HIGHERTHAN {

                @Override
                public boolean outOfBounds(double suggestedValue, double preferredValue) {
                    return preferredValue >= suggestedValue;
                }

                @Override
                public String toString() {
                    return "Higher Than Preference";
                }
            },
    
    ATMAX {
                @Override
                public boolean outOfBounds(double suggestedValue, double preferredValue) {
                    return suggestedValue > preferredValue;
                }

                @Override
                public String toString() {
                    return "Less Than or Equal to Preference";
                }
            },
    ATLEAST {

                @Override
                public boolean outOfBounds(double suggestedValue, double preferredValue) {
                    return preferredValue > suggestedValue;
                }

                @Override
                public String toString() {
                    return "Higher Than or Equal to Preference";
                }
            },
    EQUAL {

                @Override
                public boolean outOfBounds(double suggestedValue, double preferredValue) {
                    return suggestedValue != preferredValue;
                }

                @Override
                public String toString() {
                    return "Equal to Preference";
                }
            };

    public abstract boolean outOfBounds(double suggestedValue, double preferredValue);

    @Override
    public abstract String toString();
}
