/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author ancla
 */
public class MultiplicationModel implements EnvironmentModel {

    private final UUID id;
    private final List<ControleumValueList> coefficients;

    public MultiplicationModel(UUID id, List<ControleumValueList> coefficients) {
        this.id = id;
        this.coefficients = coefficients;
    }

    @Override
    public List<ControleumValueList> calculate(List<ControleumValueList> values) {
        if (coefficients.size() == 1) {
            return values.stream()
                    .map((val) -> val.multipliedWith(coefficients.get(0)))
                    .collect(Collectors.toList());
        } else {
            return IntStream.range(0, values.size())
                    .mapToObj(i -> {
                        ControleumValueList n1 = values.get(i);
                        ControleumValueList n2 = coefficients.get(i);
                        return n2 == null ? n1 : n1.multipliedWith(n2);
                    }
                    )
                    .collect(Collectors.toList());
        }

    }


    @Override
        public UUID getId() {
        return id;
    }

    @Override
        public String getName() {
        return "Multiplication Model";
    }

}
