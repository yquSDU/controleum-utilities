/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic.concern;


import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public interface EnvironmentModel {
    public List<ControleumValueList> calculate(List<ControleumValueList> vals);
    public UUID getId();
    public String getName();
}
