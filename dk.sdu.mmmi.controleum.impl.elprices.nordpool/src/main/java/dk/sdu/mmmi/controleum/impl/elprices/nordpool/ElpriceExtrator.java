package dk.sdu.mmmi.controleum.impl.elprices.nordpool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 *
 * @author jcs
 */
class ElpriceExtrator implements ResultSetExtractor<TreeMap<Date, Double>> {

    private final Date startDateTime;
    private final Date endDateTime;

    public ElpriceExtrator(Date startDateTime, Date endDateTime) {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    @Override
    public TreeMap<Date, Double> extractData(ResultSet rs) throws SQLException, DataAccessException {
        TreeMap<Date, Double> result = new TreeMap();

        //take data for each day on a list
        //if there is a result this call returns true and points at first row
        if (rs.next()) {
            do {
                //as it is pointing at the first row retrieve price per hour
                for (int i = 0; i < 24; i++) {
                    //convert the sql date and the hourly row into a java.util.Date object
                    Date sqldate = rs.getDate("Date");

                    //FIXME: There is an UTC conversion bug.
                    //The bug is observed as an hourly time shift in the datacharts.
                    //nulling the minutes, seconds and millis of day
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(sqldate.getTime());

                    //set the hour
                    cal.set(Calendar.HOUR_OF_DAY, i);

                    //transform to java.util.Date
                    Date date = new Date(cal.getTimeInMillis());

                    //dont include timevaluepoints outside periode
                    if (startDateTime.getTime() <= date.getTime()
                            && date.getTime() <= endDateTime.getTime()) {
                        //get the price of the hour
                        Double hourlyprice = rs.getDouble(i + 3);

                        //create DoubleTimeValuePoint
                        //put into list
                        result.put(date, hourlyprice);
                    }

                }
            } while (rs.next());
        }
        return result;
    }
}
