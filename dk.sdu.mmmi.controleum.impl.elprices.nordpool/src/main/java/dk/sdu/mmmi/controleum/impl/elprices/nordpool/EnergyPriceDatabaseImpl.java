package dk.sdu.mmmi.controleum.impl.elprices.nordpool;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceDatabaseException;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergyPriceService;
import dk.sdu.mmmi.controleum.api.electricityprices.EnergySpotPriceService;
import dk.sdu.mmmi.controleum.impl.entities.config.EnergyPriceDatabaseArea;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.HOUR_IN_MS;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfDayWeekBefore;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.timeWeekBefore;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.openide.util.lookup.ServiceProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author jemr, jcs
 */
@ServiceProvider(service = EnergyPriceService.class)
public class EnergyPriceDatabaseImpl implements EnergySpotPriceService, Disposable {

    private final AnnotationConfigApplicationContext moduleContext;
    private final JdbcTemplate jdbcTemplate;

    public EnergyPriceDatabaseImpl() {
        moduleContext = new AnnotationConfigApplicationContext(EnergyPriceDatabaseConfig.class);
        jdbcTemplate = moduleContext.getBean(JdbcTemplate.class);
    }

    @Override
    public Map<Date, Double> getPrices(EnergyPriceDatabaseArea area, final Date start, final Date end) throws EnergyPriceDatabaseException {

        TreeMap<Date, Double> result = jdbcTemplate.query(
                "SELECT * FROM nordpool_spot_dkr WHERE date >= ? and date <= ? and area = ?",
                new ElpriceExtrator(start, end),
                new java.sql.Date(start.getTime()),
                new java.sql.Date(end.getTime()),
                area.getDBName());

        // If the result does not have 3 days data then fill in
        // with the timeslots from previous week
        if (0 < result.size() && result.size() <= 24 * 3) { //qqq uncomment
            result.putAll(getFillData(area, result.lastKey(), end));
        }

        return result;
    }

    private Map<Date, Double> getFillData(EnergyPriceDatabaseArea area, final Date start, final Date end) throws DataAccessException {

        Date s = timeWeekBefore(start);
        Date e = endOfDayWeekBefore(end);

        TreeMap<Date, Double> lastestDayData = jdbcTemplate.query(
                "SELECT * FROM nordpool_spot_dkr WHERE date > ? and date <= ? and area = ?",
                new ElpriceExtrator(s, e),
                new java.sql.Date(s.getTime()),
                new java.sql.Date(e.getTime()),
                area.getDBName());

        Map<Date, Double> fillData = new TreeMap();

        long newHourTime = start.getTime();

        // TODO: Here is a bug when time is shifting from winter to summer time
        for (Map.Entry<Date, Double> entry : lastestDayData.entrySet()) {
            newHourTime += HOUR_IN_MS;
            fillData.put(new Date(newHourTime), entry.getValue());
        }

        return fillData;
    }

    @Override
    public void dispose() {
        moduleContext.close();
    }
//    private long setToMidnight(long time) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(time);
//
//        //set it to 00:00:00'000
//        cal.set(Calendar.HOUR_OF_DAY, 00);
//        cal.set(Calendar.MINUTE, 00);
//        cal.set(Calendar.SECOND, 00);
//        cal.set(Calendar.MILLISECOND, 00);
//
//        return cal.getTimeInMillis();
//    }
//
//    private long setToLastSecondInDay(long time) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(time);
//
//        //set it to 23:59:59'000
//        cal.set(Calendar.HOUR_OF_DAY, 23);
//        cal.set(Calendar.MINUTE, 59);
//        cal.set(Calendar.SECOND, 59);
//        cal.set(Calendar.MILLISECOND, 999);
//
//        return cal.getTimeInMillis();
//    }
//    private String sqlInsertElPrices(String currency) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("INSERT INTO ");
//        if (currency.equals("EUR")) {
//            sb.append("nordpool_spot");
//        } else {
//            sb.append("nordpool_spot_dkr");
//        }
//        sb.append("(date, area, ");
//        for (Hour slot : values()) {
//            sb.append(slot);
//            if (slot.equals(Slot_23)) {
//                sb.append(")");
//            } else {
//                sb.append(", ");
//            }
//        }
//        sb.append("VALUES(?,?,");
//        for (Hour slot : Hour.values()) {
//            if (slot.equals(Slot_23)) {
//                sb.append("?)");
//            } else {
//                sb.append("?, ");
//            }
//        }
//        return sb.toString();
//    }
//
//    private String sqlInsertTransportationPrices() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("INSERT INTO energinet_transportation_prices ");
//        sb.append("(quarter, \"year\", area, nettarif, systarif, pso) ");
//        sb.append("VALUES (?, ?, ?, ?, ?, ?)");
//        return sb.toString();
//    }
//
//    private String sqlSelectElPricesPeriod() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("SELECT * ");
//        sb.append("FROM nordpool_spot ");
//        sb.append("WHERE date >= ? and date <= ? and area = ?");
//        return sb.toString();
//    }
//
//    private String sqlSelectElPrices() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("SELECT * ");
//        sb.append("FROM nordpool_spot ");
//        sb.append("WHERE date = ? AND area = ?");
//        return sb.toString();
//    }
//
//    private String selectTransPricesSQL() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("SELECT * ");
//        sb.append("FROM energinet_transportation_prices ");
//        sb.append("WHERE \"year\" = ? AND Quarter = ? AND Area = ?");
//        return sb.toString();
//    }
//
//    private String sqlCreateTransTable() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("CREATE TABLE  energinet_transportation_prices (");
//        sb.append("quarter VARCHAR(6) NOT NULL,");
//        sb.append("\"year\" VARCHAR(4) NOT NULL,");
//        sb.append("area VARCHAR(6) NOT NULL,");
//        sb.append("netTarif DOUBLE NOT NULL,");
//        sb.append("sysTarif DOUBLE NOT NULL,");
//        sb.append("pso DOUBLE NOT NULL)");
//        return sb.toString();
//    }
//
//    private String sqlCreateExchangeTable() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("CREATE TABLE dkrexchangerate (");
//        sb.append("date DATE NOT NULL,");
//        sb.append("final VARCHAR(6) NOT NULL,");
//        sb.append("exchange_Rate DOUBLE NOT NULL");
//        sb.append(")");
//        return sb.toString();
//    }
//
//    private String sqlCreateNordPoolTable(String currency) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("CREATE TABLE ");
//        if (currency.equals("EUR")) {
//            sb.append("nordpool_spot (");
//        } else {
//            sb.append("nordpool_spot_dkr (");
//        }
//
//        sb.append("date DATE NOT NULL,");
//        sb.append("area VARCHAR(6) NOT NULL,");
//        for (Hour slot : Hour.values()) {
//            sb.append(slot);
//            if (slot.equals(Slot_23)) {
//                sb.append(" DECIMAL(8,2))");
//            } else {
//                sb.append(" DECIMAL(8,2),");
//            }
//        }
//        return sb.toString();
//    }
//
//    private String sqlInsertExchangeRate() {
//        StringBuilder sb = new StringBuilder("INSERT INTO dkrexchangerate ");
//        sb.append(" (date, final, exchange_Rate)");
//        sb.append("VALUES(?,?,?)");
//        return sb.toString();
//    }
}
